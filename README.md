# trav's OpenMW Journal Enhanced

This is a proof of concept for a modded OpenMW game journal, offering size customization, filtering topics/quests, grouping quests by location, few navigation tweaks.

https://www.youtube.com/watch?v=Jr_NFc-cY8g

Proof of concept, because even though it's a functionally complete journal rewrite, it needs custom Lua API bindings in openmw code to work for any real game. Specifically it needs this https://gitlab.com/trav55/openmw/-/commit/d13d920e4f5f30230c7f53e39ecc4d749c8d9c34 - which I'm not merge requesting until I'm sure this is a good design. I may have to think of something better.

The mod can be playtested on a 0.49 openmw main version using predefined journal data I added. Simply go to the ingame Options -> Scripts -> "trav's OpenMW Journal Enhanced" -> Development settings -> "Use sample journal data" and choose either small or huge data.

And there are still other TODOs, like:
- Maybe make some high-res textures
- Write this README properly :)

