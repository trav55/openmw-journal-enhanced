local util = require('openmw.util')
local ui = require('openmw.ui')
local I = require('openmw.interfaces')
local async = require('openmw.async')
local constants = require("scripts.openmw_journal_enhanced.ui_layout.ui_constants")
local tooltip = require("scripts.openmw_journal_enhanced.outside_manipulators.tooltip")
local ui_text = require("scripts.openmw_journal_enhanced.ui_layout.ui_text")
local l10n = require('openmw.core').l10n("openmw_journal_enhanced")
local settings = require("scripts.openmw_journal_enhanced.settings")
local templates = require("scripts.openmw_journal_enhanced.ui_layout.ui_templates")
local quest_access_from_headers = require(
    "scripts.openmw_journal_enhanced.outside_manipulators.quick_quest_access_from_main_journal")
local quest_location_naming = require("scripts.openmw_journal_enhanced.outside_manipulators.quest_location_naming")
local quest_stage_editing = require("scripts.openmw_journal_enhanced.outside_manipulators.quest_stage_editing")

local function makeQuestEntryHeaderText(questData, fallbackOutput)
    if questData ~= nil and questData.name ~= nil and questData.name ~= "" then
        return questData.name
    elseif fallbackOutput ~= nil then
        return fallbackOutput
    else
        return "ERROR_QUEST_ID_EXPECTED"
    end
end

local function createContextMenuQuestNameHeader(questId, gameData)
    local questContextText = l10n("Quest") ..
        ": \"" .. makeQuestEntryHeaderText(gameData.questEntries[questId], questId) .. "\""
    if settings.displayQuestIdInQuestList() then
        questContextText = questContextText .. " [" .. questId .. "]"
    end

    local result = ui_text.createNormalTextWidget(questContextText)
    result.content = ui.content(
        {
            { template = templates.paperColoredUnderline }
        })
    result.props.textColor = constants.paperLikeColorLighter
    result.events = {
        focusGain = async:callback(function(e)
            tooltip.killTooltip()
        end),
    }
    return result
end

local contextMenu = nil

local function calculateContextMenuPosition(mousePosition)
    return util.vector2(mousePosition.x + constants.contextMenuOffset.x, mousePosition.y + constants.contextMenuOffset.y)
end

local function updateContextMenu()
    tooltip.killTooltip()
    if contextMenu ~= nil then
        contextMenu:update()
    end
end

local CQ = {}

local function createContextMenuWidget()
    local result = ui.create(
        {
            layer = 'Windows',
            type = ui.TYPE.Image,
            props = {
                relativeSize = util.vector2(1, 1),
            },
            content = ui.content({
                {
                    name = "contextMenuWindowBlocker",
                    type = ui.TYPE.Image,
                    props = {
                        relativeSize = util.vector2(1.0, 1.0),
                        resource = constants.whiteTexture,
                        alpha = 0.0,
                    },
                    events = {
                        focusGain = async:callback(function(e)
                            tooltip.killTooltip()
                        end),
                        mouseRelease = async:callback(function(e, thisObject)
                            CQ.killContextMenu()
                        end),
                    }
                },
                {
                    name = "contextMenuBorders",
                    template = I.MWUI.templates.boxTransparent,
                    props = {
                        position = util.vector2(0.0, 0.0),
                    },
                    content = ui.content {
                        {
                            name = "contextMenuPaddingContainer",
                            template = I.MWUI.templates.padding,
                            content = ui.content {
                                {
                                    name = "contextMenuLineArranger",
                                    type = ui.TYPE.Flex,
                                    props = {
                                        autoSize = true,
                                        horizontal = false,
                                    },
                                },
                            },
                        },
                    },
                }
            })
        })
    return result
end

local function createContextMenuOptionToJumpToQuestEntries(questId, journalWindow)
    return ui_text.createContextMenuOptionWidget(
        l10n("ContextMenuShowQuestEntries"),
        function()
            quest_access_from_headers.tryJumpingToQuest(journalWindow, questId)
            CQ.killContextMenu()
        end,
        updateContextMenu
    )
end

local function createContextMenuOptionToEditQuestStartingLocation(questId, journalWindow)
    return ui_text.createContextMenuOptionWidget(
        l10n("ContextMenuEditStartingLocation"),
        function()
            quest_location_naming.openQuestRenameWindow(journalWindow, questId)
            CQ.killContextMenu()
        end,
        updateContextMenu
    )
end

local function createContextMenuOptionToEditQuestStage(questId, journalWindow)
    return ui_text.createContextMenuOptionWidget(
        l10n("ContextMenuEditQuestStage"),
        function()
            quest_stage_editing.openQuestStageEditWindow(journalWindow, questId)
            CQ.killContextMenu()
        end,
        updateContextMenu
    )
end

function CQ.createContextMenu(mouseEvent, thisObject, journalWindow)
    CQ.killContextMenu()
    if thisObject.userData == nil or thisObject.userData.tooltipData == nil or thisObject.userData.tooltipData.args == nil then
        return
    end

    contextMenu = createContextMenuWidget()

    local isJournalHeaderForQuestEntry = (thisObject.userData.tooltipData.args.questId ~= nil)
    local isBookmarkQuestName = (thisObject.userData.tooltipData.args.isQuestListQuestElement == true)

    local contextMenuOptions = {}

    if isJournalHeaderForQuestEntry then
        table.insert(contextMenuOptions,
            createContextMenuQuestNameHeader(
                thisObject.userData.tooltipData.args.questId,
                journalWindow.userData.gameData))
        table.insert(contextMenuOptions,
            createContextMenuOptionToJumpToQuestEntries(
                thisObject.userData.tooltipData.args.questId,
                journalWindow))
        table.insert(contextMenuOptions, createContextMenuOptionToEditQuestStartingLocation(
            thisObject.userData.tooltipData.args.questId,
            journalWindow))
        if settings.isQuestStageEditingEnabled() then
            table.insert(
                contextMenuOptions,
                createContextMenuOptionToEditQuestStage(
                    thisObject.userData.tooltipData.args.questId,
                    journalWindow))
        end
    elseif isBookmarkQuestName then
        table.insert(contextMenuOptions,
            createContextMenuQuestNameHeader(
                thisObject.userData.questId,
                journalWindow.userData.gameData))
        table.insert(contextMenuOptions, createContextMenuOptionToEditQuestStartingLocation(
            thisObject.userData.questId,
            journalWindow))
        if settings.isQuestStageEditingEnabled() then
            table.insert(
                contextMenuOptions,
                createContextMenuOptionToEditQuestStage(
                    thisObject.userData.questId,
                    journalWindow))
        end
    end

    contextMenu.layout.content.contextMenuBorders.content.contextMenuPaddingContainer.content.contextMenuLineArranger.content =
        ui.content(contextMenuOptions)
    contextMenu.layout.content.contextMenuBorders.props.position = calculateContextMenuPosition(mouseEvent.position)
    updateContextMenu()
end

function CQ.killContextMenu()
    if contextMenu == nil then return end
    contextMenu:destroy()
    contextMenu = nil
end

return CQ
