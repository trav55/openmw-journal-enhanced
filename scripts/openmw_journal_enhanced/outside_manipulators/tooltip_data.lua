local TDF = {}

function TDF.makeTooltipData(primaryText, secondaryText, args)
    return {
        primaryText = primaryText,
        secondaryText = secondaryText,
        args = args
    }
end

return TDF