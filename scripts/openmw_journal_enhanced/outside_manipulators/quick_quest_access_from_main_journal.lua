local QCA = {}

function QCA.tryJumpingToQuest(journalWindow, questId)
    if not journalWindow.props.visible
        or journalWindow.userData.callbacks.changeJournalToSingleQuestView == nil
        or journalWindow.userData.callbacks.updateJournalWindow == nil
    then
        return
    end

    journalWindow.userData.callbacks.changeJournalToSingleQuestView({ userData = { questId = questId } })
    journalWindow.userData.callbacks.updateJournalWindow()
end

return QCA
