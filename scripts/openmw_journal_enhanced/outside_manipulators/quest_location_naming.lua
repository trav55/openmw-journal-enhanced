local ui = require('openmw.ui')
local window_sizer = require('scripts.openmw_journal_enhanced.ui_layout.window_sizer')
local util = require('openmw.util')
local journal_button = require('scripts.openmw_journal_enhanced.window.journal_button')
local non_engine_data = require("scripts.openmw_journal_enhanced.gamedata.non_engine_data")
local async = require('openmw.async')
local constants = require("scripts.openmw_journal_enhanced.ui_layout.ui_constants")
local templates = require("scripts.openmw_journal_enhanced.ui_layout.ui_templates")
local l10n = require('openmw.core').l10n("openmw_journal_enhanced")
local I = require('openmw.interfaces')
local content_idx = require("scripts.openmw_journal_enhanced.window.content_indices")
local engine_data = require("scripts.openmw_journal_enhanced.gamedata.engine_data")

local questRenameWindow = nil

local Q = {}

local function createQuestLocationNameTextInput(originalStartingLocation)
    local result = {
        name = "inputWriteQuestText",
        template = templates.filteringInputTextBox,
        props = {
            text = originalStartingLocation or "",
            multiline = false,
            relativeSize = util.vector2(1, 1),
            autoSize = true,
        },
    }
    result.events = {
        textChanged = async:callback(function(text)
            result.props.text = text
        end),
    }
    return result
end

local function createOkButton(callbacks)
    local result = journal_button.createJournalButton(
        callbacks.applyQuestLocationNameChange,
        callbacks.updateQuestRenamingWindow)
    result.userData.textColorIdle = constants.paperLikeColor
    result.userData.textColorOver = constants.paperLikeColorLighter
    result.userData.textColorPressed = constants.fontColorJournalButtonPressed
    result.props = {
        textColor = constants.paperLikeColor,
        text = "[" .. l10n("ConfirmChange") .. "]"
    }
    return result
end

local function createQuestRenameWindowTextLine(widgetName, text, textColor)
    return {
        name = widgetName,
        type = ui.TYPE.Text,
        props = {
            textAlignH = ui.ALIGNMENT.Center,
            textSize = constants.textJournalInputSize,
            textColor = textColor,
            text = text
        }
    }
end

local function createQuestRenameWindow(questName, originalStartingLocation, applyNewStartingLocation)
    return ui.create({
        layer = 'Windows',
        type = ui.TYPE.Image,
        props = {
            size = window_sizer.calculateJournalWindowSize(),
            relativePosition = util.vector2(0.5, 0.5),
            anchor = util.vector2(0.5, 0.5),
            visible = false,
        },
        content = ui.content({
            {
                name = "questRenameWindowBlocker",
                type = ui.TYPE.Image,
                props = {
                    relativeSize = util.vector2(1.0, 1.0),
                    resource = constants.whiteTexture,
                    alpha = 0.0,
                },
                events = {
                    mouseClick = async:callback(function(e)
                        Q.removeQuestRenameWindow()
                    end)
                }
            },
            {
                name = "questRenameWindowBlackBackground",
                template = I.MWUI.templates.boxTransparent,
                props = {
                    relativePosition = util.vector2(0.5, 0.5),
                    anchor = util.vector2(0.5, 0.5),
                },
                content = ui.content({
                    {
                        name = "questRenameWindowArranger",
                        type = ui.TYPE.Flex,
                        props = {
                            autoSize = true,
                            horizontal = false,
                            align = ui.ALIGNMENT.Center,
                            arrange = ui.ALIGNMENT.Center,
                        },
                        content = ui.content({
                            createQuestRenameWindowTextLine(
                                "inputWriteQuestHeader",
                                l10n("QuestLocationRenameHeader"),
                                constants.paperLikeColor),
                            createQuestRenameWindowTextLine(
                                "inputWriteQuestHeaderQuestNameDisplay",
                                "\"" .. questName .. "\"",
                                constants.fontColorInactivePressed),
                            {
                                name = "inputWriteQuestTextContainer",
                                type = ui.TYPE.Image,
                                props = {
                                    size = util.vector2(
                                        constants.textJournalInputSize * 15,
                                        constants.textJournalInputSize),
                                    anchor = util.vector2(0.5, 0.5),
                                    resource = ui.texture { path = 'white' },
                                    color = constants.paperLikeColor,
                                },
                                content = ui.content({
                                    createQuestLocationNameTextInput(originalStartingLocation),
                                }),
                            },
                            createQuestRenameWindowTextLine(
                                "inputWriteQuestFooterSayOk",
                                l10n("QuestLocationRenameFooterSayOk"),
                                constants.paperLikeColor),
                            createQuestRenameWindowTextLine(
                                "inputWriteQuestFooterSayOrCancel",
                                l10n("QuestLocationRenameFooterOrCancel"),
                                constants.paperLikeColor),
                            createOkButton({
                                applyQuestLocationNameChange = function(thisObject)
                                    applyNewStartingLocation()
                                end,
                                updateQuestRenamingWindow = function()
                                    if questRenameWindow then
                                        questRenameWindow:update()
                                    end
                                end
                            })
                        }),
                    },
                }),
            },
        })
    })
end

function Q.openQuestRenameWindow(journalWindow, questId)
    local questNonEngineData =
        journalWindow.userData.gameData.getNonEngineDataForQuest(questId)
    local originalStartingLocation = ""
    if questNonEngineData and questNonEngineData.startingLocation then
        originalStartingLocation = questNonEngineData.startingLocation
    end

    local questName = engine_data.questName({ id = questId })
    if questName == nil then
        questName = questId
    end

    local function applyNewStartingLocation()
        if questRenameWindow == nil then
            Q.removeQuestRenameWindow()
            journalWindow.userData.callbacks.updateJournalWindow()
            return
        end
        local newName = questRenameWindow.layout.content.questRenameWindowBlackBackground.content
            .questRenameWindowArranger.content.inputWriteQuestTextContainer.content.inputWriteQuestText
            .props.text
        non_engine_data.applyNewQuestStartingLocation(
            journalWindow.userData.gameData.nonEngineData,
            questId,
            newName)
        Q.removeQuestRenameWindow()
        journalWindow.content[content_idx.JOURNAL.journalBookmark].userData.callbacks.onQuestsClick()
        journalWindow.userData.callbacks.updateJournalWindow()
    end

    if questRenameWindow then
        Q.removeQuestRenameWindow()
    else
        questRenameWindow = createQuestRenameWindow(
            questName,
            originalStartingLocation,
            applyNewStartingLocation)
        questRenameWindow.layout.props.visible = true
        questRenameWindow:update()
    end
end

function Q.removeQuestRenameWindow()
    if questRenameWindow and questRenameWindow.layout and questRenameWindow.layout.props then
        questRenameWindow.layout.props.visible = false
        questRenameWindow:destroy()
        questRenameWindow = nil
    end
end

return Q
