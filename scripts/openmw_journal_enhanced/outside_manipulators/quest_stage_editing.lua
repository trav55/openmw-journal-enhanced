local ui = require('openmw.ui')
local window_sizer = require('scripts.openmw_journal_enhanced.ui_layout.window_sizer')
local util = require('openmw.util')
local journal_button = require('scripts.openmw_journal_enhanced.window.journal_button')
local async = require('openmw.async')
local constants = require("scripts.openmw_journal_enhanced.ui_layout.ui_constants")
local templates = require("scripts.openmw_journal_enhanced.ui_layout.ui_templates")
local l10n = require('openmw.core').l10n("openmw_journal_enhanced")
local I = require('openmw.interfaces')
local engine_data = require("scripts.openmw_journal_enhanced.gamedata.engine_data")
local settings = require("scripts.openmw_journal_enhanced.settings")
local ambient = require('openmw.ambient')

local questStageEditWindow = nil

local paddingRelative = 0.01

local function updateQuestStageEditWindow()
    if questStageEditWindow ~= nil then
        questStageEditWindow:update()
    end
end

local Q = {}

local function createOkButton(callbacks)
    local result = journal_button.createJournalButton(
        callbacks.confirmQuestStageChanges,
        updateQuestStageEditWindow)
    result.userData.textColorIdle = constants.paperLikeColor
    result.userData.textColorOver = constants.paperLikeColorLighter
    result.userData.textColorPressed = constants.fontColorJournalButtonPressed
    result.props = {
        textColor = constants.paperLikeColor,
        text = "[" .. l10n("ConfirmChange") .. "]",
    }
    return result
end

local function createScrollButton(direction, callbacks, updateCallback)
    local result = {
        template = I.MWUI.templates.box,
        content = ui.content({
            {
                type = ui.TYPE.Image,
                props = {
                    resource = ui.texture { path = 'textures/omw_menu_scroll_' .. direction .. '.dds' },
                    size = util.vector2(1, 1) * constants.bookmarkScrollButtonSize,
                },
            },
        }),
        events = {
            mousePress = async:callback(function(e, thisObject)
                ambient.playSound("menu click")
            end)
        }
    }

    if direction == "up" then
        result.events.mouseRelease = async:callback(function(e, thisObject)
            callbacks.scrollUp()
            updateCallback()
        end)
    else
        result.events.mouseRelease = async:callback(function(e, thisObject)
            callbacks.scrollDown()
            updateCallback()
        end)
    end

    return result
end

local function createScrollElevatorBar()
    return {
        type = ui.TYPE.Image,
        external = {
            grow = 1
        },
        props = {
            resource = ui.texture { path = 'textures/omw_menu_scroll_center_v.dds' },
            size = util.vector2(1, 1) * (constants.bookmarkScrollButtonSize * 0.6),
        },
    }
end

local function switchSelectedQuestStage(questId, questStage)
    if questStageEditWindow == nil then return end
    for _, editedQuestTable in pairs(
        questStageEditWindow.layout.content.questStageEditWindowBlackBackground.content.questStageEditWindowSpace.content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.content
    ) do
        if editedQuestTable.content then
            for _, tableRow in pairs(editedQuestTable.content) do
                if tableRow.content
                    and #tableRow.content > 0
                    and tableRow.content[1].userData ~= nil
                    and tableRow.content[1].userData.stageSelected ~= nil
                    and tableRow.content[1].userData.questId == questId
                then
                    if tableRow.content[1].userData.questStage == questStage then
                        tableRow.content[1].userData.stageSelected = true
                        tableRow.content[1].content[1].content[1].props.alpha = 1.0
                        questStageEditWindow.layout.userData.changesToApply[questId] = {
                            stage = questStage,
                            finishesQuest = tableRow.content[1].userData.finishesQuest
                        }
                    else
                        tableRow.content[1].userData.stageSelected = false
                        tableRow.content[1].content[1].content[1].props.alpha = 0.0
                    end
                end
            end
        end
    end
end

local function applyChangesInQuestStages()
    if questStageEditWindow == nil then return end
    local changedAnything = false
    for questId, changes in pairs(questStageEditWindow.layout.userData.changesToApply) do
        local currentPlayerStageInThisQuest = engine_data.quests()[questId].stage
        if changes.stage ~= currentPlayerStageInThisQuest then
            engine_data.quests()[questId].stage = changes.stage
            engine_data.quests()[questId].finished = changes.finishesQuest
            engine_data.quests()[questId]:addJournalEntry(changes.stage)
            changedAnything = true
        end
    end
    if changedAnything then
        I.UI.removeMode("Journal")
    else
        Q.removeQuestStageEditWindow()
    end
end

local function createQuestStageEditWindow()
    local result = ui.create({
        layer = 'Windows',
        type = ui.TYPE.Image,
        props = {
            size = window_sizer.calculateJournalWindowSize(),
            relativePosition = util.vector2(0.5, 0.5),
            anchor = util.vector2(0.5, 0.5),
            visible = false,
        },
        content = ui.content({
            {
                name = "questStageEditWindowBlocker",
                type = ui.TYPE.Image,
                props = {
                    relativeSize = util.vector2(1.0, 1.0),
                    resource = constants.whiteTexture,
                    alpha = 0.0,
                },
                events = {
                    mouseClick = async:callback(function(e)
                        Q.removeQuestStageEditWindow()
                    end)
                }
            },
            {
                name = "questStageEditWindowBlackBackground",
                template = I.MWUI.templates.boxSolid,
                props = {
                    relativePosition = util.vector2(0.5, 0.5),
                    anchor = util.vector2(0.5, 0.5),
                },
            },
        })
    })
    local function scrollDown()
        if result.layout.content.questStageEditWindowBlackBackground.content.questStageEditWindowSpace.content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.props then
            result.layout.content.questStageEditWindowBlackBackground.content.questStageEditWindowSpace.content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.props.anchor =
                util.vector2(
                    0.0,
                    math.min(
                        1.0,
                        result.layout.content.questStageEditWindowBlackBackground.content.questStageEditWindowSpace
                        .content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.props
                        .anchor.y + result.layout.userData.scrollRatio)
                )
            result.layout.content.questStageEditWindowBlackBackground.content.questStageEditWindowSpace.content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.props.relativePosition =
                util.vector2(
                    0.0,
                    math.min(
                        1.0,
                        result.layout.content.questStageEditWindowBlackBackground.content.questStageEditWindowSpace
                        .content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.props
                        .relativePosition.y + result.layout.userData.scrollRatio)
                )
        end
    end

    local function scrollUp()
        if result.layout.content.questStageEditWindowBlackBackground.content.questStageEditWindowSpace.content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.props then
            result.layout.content.questStageEditWindowBlackBackground.content.questStageEditWindowSpace.content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.props.anchor =
                util.vector2(
                    0.0,
                    math.max(0.0,
                        result.layout.content.questStageEditWindowBlackBackground.content.questStageEditWindowSpace
                        .content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.props
                        .anchor.y - result.layout.userData.scrollRatio)
                )
            result.layout.content.questStageEditWindowBlackBackground.content.questStageEditWindowSpace.content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.props.relativePosition =
                util.vector2(
                    0.0,
                    math.max(0.0,
                        result.layout.content.questStageEditWindowBlackBackground.content.questStageEditWindowSpace
                        .content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.props
                        .relativePosition.y - result.layout.userData.scrollRatio)
                )
        end
    end

    result.layout.userData =
    {
        scrollRatio = settings.bookmarkScrollRatio(),
        callbacks =
        {
            scrollUp = scrollUp,
            scrollDown = scrollDown,
            confirmQuestStageChanges = applyChangesInQuestStages,
            selectStageForQuest = switchSelectedQuestStage
        },
        changesToApply = {},
    }

    result.layout.content.questStageEditWindowBlackBackground.content = ui.content({
        {
            name = "questStageEditWindowSpace",
            props =
            {
                size = util.vector2(
                    result.layout.props.size.x * 0.6,
                    result.layout.props.size.y * 0.8),
            },
        }
    })

    local topRowHeaderText = {
        name = "headerText",
        type = ui.TYPE.Text,
        props = {
            relativeSize = util.vector2(0.8, 0.17),
            relativePosition = util.vector2(paddingRelative, paddingRelative),
            textAlignH = ui.ALIGNMENT.Start,
            autoSize = false,
            wordWrap = true,
            multiline = true,
            textSize = constants.textContextMenuOptionSize,
            textColor = constants.paperLikeColor,
            text = l10n("questStageEditWindowHeaderTest")
        }
    }

    local confirmButton = createOkButton(result.layout.userData.callbacks)
    confirmButton.name = "confirmButton"
    confirmButton.props.relativePosition = util.vector2(0.9, 0.06)

    local scrollbarRelativeWidth = 0.04
    local contentLimitingTheShiftingSpace = {
        name = "contentLimitingTheShiftingSpace",
        props = {
            relativeSize = util.vector2(
                1.0 - (2 * paddingRelative) - scrollbarRelativeWidth,
                0.85 - paddingRelative),
            relativePosition = util.vector2(paddingRelative, 0.15),
        },
        content = ui.content({
            {
                name = "spaceToBeShiftedWhenScrolling",
                type = ui.TYPE.Flex,
                props = {
                    horizontal = false,
                    autoSize = true,
                    arrange = ui.ALIGNMENT.Center,
                    anchor = util.vector2(0.0, 0.0),
                    relativePosition = util.vector2(0.0, 0.0),
                },
                content = ui.content({}),
            }
        }),
    }

    local contentWithScrollBar =
    {
        name = "contentWithScrollBar",
        type = ui.TYPE.Flex,
        props = {
            horizontal = false,
            autoSize = false,
            arrange = ui.ALIGNMENT.Center,
            anchor = util.vector2(1.0, 0),
            relativeSize = util.vector2(
                scrollbarRelativeWidth,
                0.85 - paddingRelative),
            relativePosition = util.vector2(1.0 - paddingRelative, 0.15),
        },
        content = ui.content({
            createScrollButton("up", result.layout.userData.callbacks, updateQuestStageEditWindow),
            createScrollElevatorBar(),
            createScrollButton("down", result.layout.userData.callbacks, updateQuestStageEditWindow),
        }),
    }

    result.layout.content.questStageEditWindowBlackBackground.content.questStageEditWindowSpace.content =
        ui.content({
            topRowHeaderText,
            confirmButton,
            contentLimitingTheShiftingSpace,
            contentWithScrollBar,
        })

    return result
end

local function populateQuestDataWithQuestTitle(questId)
    if questStageEditWindow == nil then return end
    local questName = engine_data.questName({ id = questId })
    if questName == nil then
        questName = questId
    end

    local questContextText = "\"" .. questName .. "\"" .. " [" .. questId .. "]"

    local questNameTitle = {
        type = ui.TYPE.Text,
        props = {
            textSize = constants.textContextMenuOptionSize,
            textColor = constants.paperLikeColorLighter,
            text = questContextText
        },
        content = ui.content({ { template = templates.paperColoredUnderline } })
    }
    table.insert(
        questStageEditWindow.layout.content.questStageEditWindowBlackBackground.content.questStageEditWindowSpace
        .content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.content,
        questNameTitle)
end

local function makeQuestStageEditTableRows(questId)
    if questStageEditWindow == nil then return end
    local function makeTableRow(line)
        return {
            type = ui.TYPE.Flex,
            props = {
                size = util.vector2(0, 0),
                autoSize = true,
                horizontal = true,
            },
            content = ui.content(line)
        }
    end

    local function makeCellEntry(text, portion)
        local mainWindowWidth = questStageEditWindow.layout.content.questStageEditWindowBlackBackground.content
            .questStageEditWindowSpace.props.size.x
        local padding = (paddingRelative * mainWindowWidth)
        local width = (mainWindowWidth - (padding * 2)) * portion
        local height = constants.textContextMenuOptionSize + padding

        return {
            type = ui.TYPE.Text,
            props = {
                size = util.vector2(width, height),
                text = text,
                autoSize = false,
                textAlignH = ui.ALIGNMENT.Center,
                textSize = constants.textContextMenuOptionSize,
                textColor = constants.paperLikeColor,
            }
        }
    end

    local function makeHeaderCellEntry(text, portion)
        local result = makeCellEntry(text, portion)
        result.props.textColor = constants.paperLikeColorLighter
        return result
    end

    local cellWidths = { 0.1, 0.08, 0.22, 0.6 }

    local lines = {}

    local function makeCellEntryForJournalText(text)
        local result = makeCellEntry(text, cellWidths[4])
        result.props.textAlignH = ui.ALIGNMENT.Start
        result.props.textSize = result.props.textSize * 0.6
        result.props.multiline = true
        result.props.wordWrap = true
        return result
    end

    local function makeQuestStageSelector(questId, givenStage, finishesQuest)
        local currentPlayerStageInThisQuest = engine_data.quests()[questId].stage

        local selectorButton = {
            template = I.MWUI.templates.box,
            props = {
                relativePosition = util.vector2(0.5, 0.5),
                anchor = util.vector2(0.5, 0.5),
            },
            content = ui.content({
                {
                    type = ui.TYPE.Image,
                    props = {
                        resource = ui.texture { path = 'textures/omw_menu_scroll_right.dds' },
                        size = util.vector2(1, 1) * constants.bookmarkScrollButtonSize,
                    },
                },
            }),
            events = {
                mousePress = async:callback(function(e, thisObject)
                    ambient.playSound("menu click")
                    questStageEditWindow.layout.userData.callbacks.selectStageForQuest(
                        questId,
                        givenStage)
                    updateQuestStageEditWindow()
                end)
            }
        }


        local result = makeCellEntry(">", cellWidths[1])
        result.type = ui.TYPE.Image
        result.props = {
            size = result.props.size
        }
        result.userData = {
            questId = questId,
            questStage = givenStage,
            finishesQuest = finishesQuest,
            stageSelected = true
        }

        if givenStage ~= currentPlayerStageInThisQuest then
            selectorButton.content[1].props.alpha = 0.0
            result.userData.stageSelected = false
        end
        result.content = ui.content({ selectorButton })

        return result
    end

    for _, questEntry in pairs(engine_data.questJournalEntryRecords(questId)) do
        if not questEntry.isQuestName then
            local specialFlag = ""
            if questEntry.isQuestFinished then
                specialFlag = specialFlag .. l10n("questStageEditWindowTableCellFlagFinishedValue")
            end
            if questEntry.isQuestRestart then
                specialFlag = specialFlag .. l10n("questStageEditWindowTableCellFlagRestartedValue")
            end
            table.insert(
                lines,
                makeTableRow({
                    makeQuestStageSelector(questId, questEntry.questStage, questEntry.isQuestFinished),
                    makeCellEntry(tostring(questEntry.questStage), cellWidths[2]),
                    makeCellEntry(specialFlag, cellWidths[3]),
                    makeCellEntryForJournalText("\"" .. questEntry.text .. "\""),
                })
            )
        end
    end

    local function sortQuestEntryLinesByQuestStage(rowA, rowB)
        return rowA.content[1].userData.questStage < rowB.content[1].userData.questStage
    end
    table.sort(lines, sortQuestEntryLinesByQuestStage)

    table.insert(lines, 1, makeTableRow({
        makeHeaderCellEntry(l10n("questStageEditWindowTableHeaderSelect"), cellWidths[1]),
        makeHeaderCellEntry(l10n("questStageEditWindowTableHeaderStage"), cellWidths[2]),
        makeHeaderCellEntry(l10n("questStageEditWindowTableHeaderFlags"), cellWidths[3]),
        makeHeaderCellEntry(l10n("questStageEditWindowTableHeaderJournalEntry"), cellWidths[4]),
    }))

    return lines
end

local function populateQuestStageEditWindowWithDataForQuestId(questId)
    if questStageEditWindow == nil then return end

    local function makeTable(lines)
        return {
            type = ui.TYPE.Flex,
            props = {
                autoSize = true,
                horizontal = false,
            },
            content = ui.content(lines)
        }
    end

    local function addDataForQuestId(questIdToAdd)
        populateQuestDataWithQuestTitle(questIdToAdd)
        local lines = makeQuestStageEditTableRows(questIdToAdd)
        table.insert(
            questStageEditWindow.layout.content.questStageEditWindowBlackBackground.content.questStageEditWindowSpace
            .content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.content,
            makeTable(lines)
        )
    end

    addDataForQuestId(questId)

    local groupQuestsWithTheSameNameIntoOne = not settings.separateQuestsWithTheSameName()
    if groupQuestsWithTheSameNameIntoOne then
        local questName = engine_data.questName({ id = questId })
        if questName == nil then
            questName = questId
        end
        for _, possibleDuplicateQuest in pairs(engine_data.questJournalRecords()) do
            if possibleDuplicateQuest.questName == questName and possibleDuplicateQuest.id ~= questId then
                addDataForQuestId(possibleDuplicateQuest.id)
            end
        end
    end
end

function Q.openQuestStageEditWindow(journalWindow, questId)
    if questStageEditWindow then
        Q.removeQuestStageEditWindow()
    else
        questStageEditWindow = createQuestStageEditWindow()
        populateQuestStageEditWindowWithDataForQuestId(questId)
        questStageEditWindow.layout.props.visible = true
        updateQuestStageEditWindow()
    end
end

function Q.removeQuestStageEditWindow()
    if questStageEditWindow and questStageEditWindow.layout and questStageEditWindow.layout.props then
        questStageEditWindow.layout.props.visible = false
        questStageEditWindow:destroy()
        questStageEditWindow = nil
    end
end

function Q.handleMouseWheelEvent(x)
    if questStageEditWindow and questStageEditWindow.layout and questStageEditWindow.layout.props then
        if x > 0 then     --up
            questStageEditWindow.layout.userData.callbacks.scrollUp()
        elseif x < 0 then -- down
            questStageEditWindow.layout.userData.callbacks.scrollDown()
        end
        updateQuestStageEditWindow()
        return true
    end
    return false
end

return Q
