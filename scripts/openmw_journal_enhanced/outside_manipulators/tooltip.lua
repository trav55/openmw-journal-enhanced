local ui = require('openmw.ui')
local async = require('openmw.async')
local constants = require("scripts.openmw_journal_enhanced.ui_layout.ui_constants")
local util = require('openmw.util')
local I = require('openmw.interfaces')
local settings = require("scripts.openmw_journal_enhanced.settings")
local l10n = require('openmw.core').l10n("openmw_journal_enhanced")
local PhraseType = require("scripts.openmw_journal_enhanced.wording.phrase_type")

local tooltip = nil

local TT = {}

local function createTooltipSpace(primaryText, secondaryText)
    local result = ui.create(
        {
            layer = 'Windows',
            template = I.MWUI.templates.boxTransparent,
            props = {
                position = util.vector2(0, 0),
            },
            content = ui.content {
                {
                    name = "tooltipPaddingContainer",
                    template = I.MWUI.templates.padding,
                    content = ui.content {
                        {
                            name = "tooltipLineArranger",
                            type = ui.TYPE.Flex,
                            props = {
                                autoSize = true,
                                horizontal = false,
                            },
                        },
                    },
                },
            },
            events = {
                mouseMove = async:callback(function(mouseEvent, thisObject)
                    TT.killTooltip()
                end)
            },
        })

    local texts = {}

    if primaryText ~= nil and primaryText ~= "" then
        table.insert(
            texts,
            {
                name = "tooltipPrimaryLine",
                type = ui.TYPE.Text,
                props = {
                    textSize = constants.textJournalInputSize,
                    textColor = constants.paperLikeColor,
                    text = primaryText,
                    visible = true,
                },
            })
    end
    if secondaryText ~= nil and secondaryText ~= "" then
        table.insert(
            texts,
            {
                name = "tooltipSecondaryLine",
                type = ui.TYPE.Text,
                props = {
                    textSize = constants.textJournalInputSize * 0.8,
                    textColor = constants.fontColorJournalButtonShadow,
                    text = secondaryText,
                    multiline = true,
                    visible = true,
                },
            })
    end

    result.layout.content.tooltipPaddingContainer.content.tooltipLineArranger.content = ui.content(texts)

    return result
end

local function prepareTopicEntryForTooltip(gameData, topicData)
    local lineLimit = 40

    local tooltipSetting = settings.showTopicEntryOnHover()
    if tooltipSetting == "showTopicEntryOnHoverDisabled" then
        return nil
    end

    local entryIndexToUse = 1

    if gameData.topics[topicData.topic] ~= nil and gameData.topics[topicData.topic].entries ~= nil and #gameData.topics[topicData.topic].entries > 0 then
        if tooltipSetting == "showTopicEntryOnHoverNewest" then
            entryIndexToUse = #gameData.topics[topicData.topic].entries
        elseif tooltipSetting == "showTopicEntryOnHoverRandom" then
            entryIndexToUse = math.random(#gameData.topics[topicData.topic].entries)
        end
        local entryToDisplay = gameData.topics[topicData.topic].entries[entryIndexToUse].actor ..
            ", \"" .. gameData.topics[topicData.topic].entries[entryIndexToUse].text .. "\""
        local result = ""

        local lineStart = 1
        local lineLengthSoFar = 0
        while lineStart < #entryToDisplay do
            lineLengthSoFar = lineLengthSoFar + 1
            if (lineLengthSoFar >= lineLimit) then
                while string.byte(entryToDisplay, lineStart + lineLengthSoFar, lineStart + lineLengthSoFar) ~= 32 and lineStart + lineLengthSoFar < #entryToDisplay do --space
                    lineLengthSoFar = lineLengthSoFar + 1
                end
                if result ~= "" then
                    result = result .. "\n"
                end
                result = result .. string.sub(entryToDisplay, lineStart, lineStart + lineLengthSoFar)
                lineStart = lineStart + lineLengthSoFar + 1
                lineLengthSoFar = 0
            end
        end
        return result
    else
        return nil
    end
end

local function calculateTooltipPosition(mousePosition)
    return util.vector2(mousePosition.x + constants.tooltipOffset.x, mousePosition.y + constants.tooltipOffset.y)
end

function TT.createMouseMoveCallback(gameData)
    return async:callback(function(mouseEvent, thisObject)
        TT.killTooltip()

        if thisObject.userData == nil or thisObject.userData.tooltipData == nil then
            return
        end

        if thisObject.userData.tooltipData.args ~= nil and thisObject.userData.tooltipData.args.settingObstructingTooltip ~= nil then
            if not settings[thisObject.userData.tooltipData.args.settingObstructingTooltip]() then
                return
            end
        end

        if thisObject.userData.tooltipData.args ~= nil and thisObject.userData.tooltipData.args.topicData ~= nil then
            local possibleTopicTooltip =
                prepareTopicEntryForTooltip(gameData, thisObject.userData.tooltipData.args.topicData)
            if possibleTopicTooltip ~= nil and possibleTopicTooltip ~= "" then
                thisObject.userData.tooltipData.secondaryText = possibleTopicTooltip
            else
                return
            end
        end

        if thisObject.userData.tooltipData.args ~= nil and thisObject.userData.tooltipData.args.settingObstructingSecondaryTooltip ~= nil then
            if not settings[thisObject.userData.tooltipData.args.settingObstructingSecondaryTooltip]() then
                thisObject.userData.tooltipData.secondaryText = nil
            end
        end

        tooltip = createTooltipSpace(thisObject.userData.tooltipData.primaryText,
            thisObject.userData.tooltipData.secondaryText)

        tooltip.layout.props.position = calculateTooltipPosition(mouseEvent.position)
        tooltip:update()
    end)
end

function TT.killTooltip()
    if tooltip == nil then return end
    tooltip:destroy()
    tooltip = nil
end

return TT
