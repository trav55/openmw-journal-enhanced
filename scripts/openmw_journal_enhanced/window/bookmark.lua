local content_idx = require("scripts.openmw_journal_enhanced.window.content_indices")
local templates = require("scripts.openmw_journal_enhanced.ui_layout.ui_templates")
local l10n = require('openmw.core').l10n("openmw_journal_enhanced")
local journal_button = require('scripts.openmw_journal_enhanced.window.journal_button')
local alphabet = require('scripts.openmw_journal_enhanced.window.bookmark_alphabet')
local scrollable = require('scripts.openmw_journal_enhanced.window.bookmark_scrollable')
local text_filtering = require('scripts.openmw_journal_enhanced.window.bookmark_text_filtering')
local scrollable_content_from_game_data = require(
    "scripts.openmw_journal_enhanced.wording.scrollable_content_from_game_data")
local settings = require("scripts.openmw_journal_enhanced.settings")
local ambient = require('openmw.ambient')
local ui = require('openmw.ui')
local util = require('openmw.util')

local bookmarkCenter = 0.32
local topicsAndQuestsButtonHRelativePosition = 0.78

local function createTopicsButton(callbacks, updateJournalWindow)
    local result = journal_button.createJournalButton(callbacks.onTopicClick, updateJournalWindow)
    result.props = {
        relativePosition = util.vector2(0.15, topicsAndQuestsButtonHRelativePosition),
        text = l10n("Topics")
    }
    return result
end

local function createCancelButton(callbacks)
    local result = journal_button.createJournalButton(callbacks.toggleBookmark, callbacks.updateJournalWindow)
    result.props = {
        relativePosition = util.vector2(bookmarkCenter, 0.88),
        text = l10n("Cancel")
    }
    return result
end

local function createQuestsButton(callbacks, updateJournalWindow)
    local result = journal_button.createJournalButton(callbacks.onQuestsClick, updateJournalWindow)
    result.props = {
        relativePosition = util.vector2(0.47, topicsAndQuestsButtonHRelativePosition),
        text = l10n("Quests")
    }
    return result
end

local function createQuestFilteringButton(callbacks, updateJournalWindow)
    local result = journal_button.createJournalButton(callbacks.onFilterActiveQuestsClick, updateJournalWindow)
    result.props = {
        relativePosition = util.vector2(bookmarkCenter, 0.052)
    }
    result.userData.showAllText = l10n("ShowAll")
    result.userData.showActiveText = l10n("ShowActive")
    result.userData.isShowAllShown = true
    result.props.text = result.userData.showAllText
    return result
end

local function createFilteringInput(callbacks, updateJournalWindow)
    local result = text_filtering.createFilteringInput(callbacks, updateJournalWindow)
    result.props.relativePosition = util.vector2(bookmarkCenter, topicsAndQuestsButtonHRelativePosition - 0.045)
    return result
end

local function createQuestFilteringInformation(scrollableSpace)
    return {
        shouldShowActiveAndInactiveQuests =
            not scrollableSpace.content[content_idx.BOOKMARK.questFilteringButton].userData.isShowAllShown,
        questNameFilter = scrollableSpace.content[content_idx.BOOKMARK.filteringInput].content.inputText.props.text
    }
end

local B = {}

function B.createJournalBookmark(journalWindowUserData)
    local result = {
        template = templates.journalBookmark,
        props = {
            visible = false
        },
        content = ui.content(
            {
                [content_idx.BOOKMARK.alphabet] = alphabet.createTopicAlphabet(),
                [content_idx.BOOKMARK.scrollableSpace] = scrollable.createScrollableSpace(journalWindowUserData),
            })
    }

    local function resetQuestFilteringButton(questFilteringButton)
        questFilteringButton.props.text = l10n("ShowAll")
        questFilteringButton.props.visible = false
        questFilteringButton.userData.isShowAllShown = true
    end

    local function switchToSpecificTopicView(buttonCallingThisFunction)
        result.content[content_idx.BOOKMARK.alphabet].props.visible = false
        result.content[content_idx.BOOKMARK.scrollableSpace].props.visible = true
        resetQuestFilteringButton(result.content[content_idx.BOOKMARK.questFilteringButton])
        ambient.playSound("book page")
        scrollable.updateScrollableContent(
            result.content[content_idx.BOOKMARK.scrollableSpace],
            scrollable_content_from_game_data.generateTopicListForLetter(
                buttonCallingThisFunction.userData.letter,
                journalWindowUserData.gameData,
                journalWindowUserData.callbacks))
    end

    local function switchToQuestsViewWithoutOutsideResets()
        result.content[content_idx.BOOKMARK.alphabet].props.visible = false
        result.content[content_idx.BOOKMARK.scrollableSpace].props.visible = true
        resetQuestFilteringButton(result.content[content_idx.BOOKMARK.questFilteringButton])
        result.content[content_idx.BOOKMARK.questFilteringButton].props.visible = true
        scrollable.updateScrollableContent(
            result.content[content_idx.BOOKMARK.scrollableSpace],
            scrollable_content_from_game_data.generateQuestList(
                journalWindowUserData.gameData,
                journalWindowUserData.callbacks,
                createQuestFilteringInformation(result)))
    end

    local function switchToQuestsView()
        text_filtering.resetFilteringInput(result.content[content_idx.BOOKMARK.filteringInput])
        switchToQuestsViewWithoutOutsideResets()
        ambient.playSound("book page")
    end

    local function filterActiveQuestsView(buttonCallingThisFunction)
        result.content[content_idx.BOOKMARK.alphabet].props.visible = false
        result.content[content_idx.BOOKMARK.scrollableSpace].props.visible = true
        if buttonCallingThisFunction.userData.isShowAllShown then
            result.content[content_idx.BOOKMARK.questFilteringButton].props.text = l10n("ShowActive")
            buttonCallingThisFunction.userData.isShowAllShown = false
        else
            result.content[content_idx.BOOKMARK.questFilteringButton].props.text = l10n("ShowAll")
            buttonCallingThisFunction.userData.isShowAllShown = true
        end
        result.content[content_idx.BOOKMARK.questFilteringButton].props.visible = true
        ambient.playSound("book page")
        scrollable.updateScrollableContent(
            result.content[content_idx.BOOKMARK.scrollableSpace],
            scrollable_content_from_game_data.generateQuestList(
                journalWindowUserData.gameData,
                journalWindowUserData.callbacks,
                createQuestFilteringInformation(result)))
    end

    local function switchToAlphabetViewWithoutOutsideResets()
        result.content[content_idx.BOOKMARK.alphabet].props.visible = true
        result.content[content_idx.BOOKMARK.scrollableSpace].props.visible = false
        resetQuestFilteringButton(result.content[content_idx.BOOKMARK.questFilteringButton])
    end

    local function switchToAlphabetView()
        switchToAlphabetViewWithoutOutsideResets()
        text_filtering.resetFilteringInput(result.content[content_idx.BOOKMARK.filteringInput])
        ambient.playSound("book page")
    end

    local function applyTextFilterToQuestsOrTopics(text)
        local isOnQuestsView = (result.content[content_idx.BOOKMARK.questFilteringButton].props.visible) --hacky
        if #text < 2 and not isOnQuestsView then
            switchToAlphabetViewWithoutOutsideResets()
            return
        end

        if isOnQuestsView then
            result.content[content_idx.BOOKMARK.alphabet].props.visible = false
            result.content[content_idx.BOOKMARK.scrollableSpace].props.visible = true
            scrollable.updateScrollableContent(
                result.content[content_idx.BOOKMARK.scrollableSpace],
                scrollable_content_from_game_data.generateQuestList(
                    journalWindowUserData.gameData,
                    journalWindowUserData.callbacks,
                    createQuestFilteringInformation(result)))
        else
            result.content[content_idx.BOOKMARK.alphabet].props.visible = false
            result.content[content_idx.BOOKMARK.scrollableSpace].props.visible = true
            resetQuestFilteringButton(result.content[content_idx.BOOKMARK.questFilteringButton])
            scrollable.updateScrollableContent(
                result.content[content_idx.BOOKMARK.scrollableSpace],
                scrollable_content_from_game_data.generateTopicListForSubstring(
                    text,
                    journalWindowUserData.gameData,
                    journalWindowUserData.callbacks))
        end
    end

    local function refreshContent(gameData)
        alphabet.updateAlphabetContent(
            result.content[content_idx.BOOKMARK.alphabet],
            gameData,
            result.userData.callbacks.onAlphabetLetterClick,
            journalWindowUserData.callbacks.updateJournalWindow)
        scrollable.updateScrollableContent(result.content[content_idx.BOOKMARK.scrollableSpace], {})
        result.content[content_idx.BOOKMARK.alphabet].props.visible = true
        result.content[content_idx.BOOKMARK.scrollableSpace].props.visible = false
        text_filtering.resetFilteringInput(result.content[content_idx.BOOKMARK.filteringInput])
        resetQuestFilteringButton(result.content[content_idx.BOOKMARK.questFilteringButton])

        local startingOption = settings.startingOptionInOptions()
        if startingOption ~= "startingOptionsChoiceAlphabet" then
            switchToQuestsViewWithoutOutsideResets()
            if startingOption == "startingOptionsChoiceQuestsAll" then
                filterActiveQuestsView(result.content[content_idx.BOOKMARK.questFilteringButton])
            end
        end
    end

    local function scrollBy(x)
        if not result.content[content_idx.BOOKMARK.scrollableSpace].props.visible then
            return
        end
        if x > 0 then     --up
            result.content[content_idx.BOOKMARK.scrollableSpace].userData.callbacks.scrollUp()
        elseif x < 0 then -- down
            result.content[content_idx.BOOKMARK.scrollableSpace].userData.callbacks.scrollDown()
        end
    end

    result.userData = {
        callbacks = {
            onBookmarkUpdate = refreshContent,
            onMouseWheel = scrollBy,
            onTopicClick = switchToAlphabetView,
            onAlphabetLetterClick = switchToSpecificTopicView,
            onQuestsClick = switchToQuestsView,
            onFilterActiveQuestsClick = filterActiveQuestsView,
            applyTextFilter = applyTextFilterToQuestsOrTopics,
        }
    }

    result.content[content_idx.BOOKMARK.filteringInput] = createFilteringInput(
        result.userData.callbacks, journalWindowUserData.callbacks.updateJournalWindow)
    result.content[content_idx.BOOKMARK.questFilteringButton] = createQuestFilteringButton(
        result.userData.callbacks, journalWindowUserData.callbacks.updateJournalWindow)
    result.content[content_idx.BOOKMARK.topicsButton] = createTopicsButton(
        result.userData.callbacks, journalWindowUserData.callbacks.updateJournalWindow)
    result.content[content_idx.BOOKMARK.cancelButton] = createCancelButton(journalWindowUserData.callbacks)
    result.content[content_idx.BOOKMARK.questsButton] = createQuestsButton(
        result.userData.callbacks, journalWindowUserData.callbacks.updateJournalWindow)

    return result
end

return B
