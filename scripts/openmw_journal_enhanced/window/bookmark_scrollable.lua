local I = require('openmw.interfaces')
local util = require('openmw.util')
local ui = require('openmw.ui')
local ambient = require('openmw.ambient')
local constants = require("scripts.openmw_journal_enhanced.ui_layout.ui_constants")
local settings = require("scripts.openmw_journal_enhanced.settings")
local async = require('openmw.async')
local window_sizer = require('scripts.openmw_journal_enhanced.ui_layout.window_sizer')

local function calculateScrollRatio(scrollableContent)
    local result = 0
    local numberOfElementsInScrollableContent = #scrollableContent
    local numberOfVisibleLinesInScrollableContent = window_sizer.calculateNumberOfVisibleBookmarkTopicOrQuestLines()
    if numberOfElementsInScrollableContent > numberOfVisibleLinesInScrollableContent then
        result = (numberOfVisibleLinesInScrollableContent / numberOfElementsInScrollableContent) * settings.bookmarkScrollRatio()
    end
    return result
end

local function createScrollElevatorBar()
    return {
        type = ui.TYPE.Image,
        external = {
            grow = 1
        },
        props = {
            resource = ui.texture{ path = 'textures/omw_menu_scroll_center_v.dds' },
            size = util.vector2(1, 1) * (constants.bookmarkScrollButtonSize * 0.6),
        },
    }
end

local function createScrollButton(direction, callbacks, updateJournalWindow)
    local result = {
        template = I.MWUI.templates.box,
        content = ui.content({
            {
                type = ui.TYPE.Image,
                props = {
                    resource = ui.texture{ path = 'textures/omw_menu_scroll_'  .. direction .. '.dds' },
                    size = util.vector2(1, 1) * constants.bookmarkScrollButtonSize,
                },
            },
        }),
        events = {
            mousePress = async:callback(function(e, thisObject)
                ambient.playSound("menu click")
            end)
        }
    }

    if direction == "up" then
        result.events.mouseRelease = async:callback(function(e, thisObject)
            callbacks.scrollUp()
            updateJournalWindow()
        end)
    else
        result.events.mouseRelease = async:callback(function(e, thisObject)
            callbacks.scrollDown()
            updateJournalWindow()
        end)
    end

    return result
end

local BS = {}

function BS.createScrollableSpace(journalWindowUserData)
    local result = {
        type = ui.TYPE.Flex,
        props = {
            visible = false,
            horizontal = true,
            autoSize = false,
            size = util.vector2(0, 0),
            relativePosition = util.vector2(0.0225, 0.085),
            relativeSize = util.vector2(1, constants.bookmarkScrollableAreaHeightFactorRelativeToBookmarkHeight)
        },
        content = ui.content({
            {
                name = "contentLimitingTheShiftingSpace",
                type = ui.TYPE.Image,
                external = {
                    stretch = 1
                },
                props = {
                    relativeSize = util.vector2(0.57, 0.2),
                },
                content = ui.content({
                    {
                        name = "spaceToBeShiftedWhenScrolling",
                        type = ui.TYPE.Flex,
                        props = {
                            horizontal = false,
                            autoSize = true,
                            arrange = ui.ALIGNMENT.Start,
                            anchor = util.vector2(0.0, 0.0),
                            relativePosition = util.vector2(0.0, 0.0),
                        },
                        content = ui.content({}),
                    }
                })
            },
            {
                name = "contentWithScrollBar",
                type = ui.TYPE.Flex,
                external = {
                    stretch = 1
                },
                props = {
                    horizontal = false,
                    autoSize = false,
                    arrange = ui.ALIGNMENT.Center,
                    size = util.vector2(1, 1) * constants.bookmarkScrollButtonSize * 1.22,
                },
                content = {},
            }
        })
    }

    result.userData = {
        scrollRatio = settings.bookmarkScrollRatio()
    }

    local function scrollDown()
        if result.content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.props then
            result.content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.props.anchor =
                util.vector2(
                    0.0,
                    math.min(
                        1.0,
                        result.content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.props.anchor.y + result.userData.scrollRatio)
                )
            result.content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.props.relativePosition =
                util.vector2(
                    0.0,
                    math.min(
                        1.0,
                        result.content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.props.relativePosition.y + result.userData.scrollRatio)
                )
        end
    end

    local function scrollUp()
        if result.content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.props then
            result.content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.props.anchor =
                util.vector2(
                    0.0,
                    math.max(0.0, result.content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.props.anchor.y - result.userData.scrollRatio)
                )
            result.content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.props.relativePosition =
                util.vector2(
                    0.0,
                    math.max(0.0, result.content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.props.relativePosition.y - result.userData.scrollRatio)
                )
        end
    end

    result.userData.callbacks =
    {
        scrollUp = scrollUp,
        scrollDown = scrollDown
    }

    result.content.contentWithScrollBar.content = ui.content({
        createScrollButton("up", result.userData.callbacks, journalWindowUserData.callbacks.updateJournalWindow),
        createScrollElevatorBar(),
        createScrollButton("down", result.userData.callbacks, journalWindowUserData.callbacks.updateJournalWindow),
    })

    return result
end

function BS.updateScrollableContent(scrollableLayout, newContent)
    scrollableLayout.content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.props.anchor = util.vector2(0.0, 0.0)
    scrollableLayout.content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.props.relativePosition = util.vector2(0.0, 0.0)
    scrollableLayout.content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.content =
        ui.content(newContent)
    scrollableLayout.userData.scrollRatio = calculateScrollRatio(
        scrollableLayout.content.contentLimitingTheShiftingSpace.content.spaceToBeShiftedWhenScrolling.content)
end

return BS