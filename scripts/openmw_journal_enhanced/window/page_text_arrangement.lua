local ui = require('openmw.ui')
local util = require('openmw.util')
local ui_text = require("scripts.openmw_journal_enhanced.ui_layout.ui_text")
local constants = require("scripts.openmw_journal_enhanced.ui_layout.ui_constants")
local window_sizer = require('scripts.openmw_journal_enhanced.ui_layout.window_sizer')
local settings = require("scripts.openmw_journal_enhanced.settings")
local tooltip_data = require("scripts.openmw_journal_enhanced.outside_manipulators.tooltip_data")
local l10n = require('openmw.core').l10n("openmw_journal_enhanced")
local PhraseType = require("scripts.openmw_journal_enhanced.wording.phrase_type")

local function makeLineWidget(line)
    return {
        type = ui.TYPE.Flex,
        props = {
            size = util.vector2(0, 0),
            horizontal = true,
            relativeSize = util.vector2(1.0, 1.0)
        },
        content = ui.content( line )
    }
end

local function applyMouseMoveEventToWidget(widget, event)
    if widget.events == nil then
        widget.events = {}
    end
    widget.events.mouseMove = event
end

local function insertPhraseIntoLine(currentLine, phrase, onTopicLinkClickCallback, updateJournalWindow, onJournalPageContentMouseMove)
    if phrase.userData.type == PhraseType.TOPIC then
        local topicPhrase = ui_text.createLinkTextWidget(phrase.props.text, onTopicLinkClickCallback, updateJournalWindow)
        applyMouseMoveEventToWidget(topicPhrase, onJournalPageContentMouseMove)
        topicPhrase.userData.tooltipData = tooltip_data.makeTooltipData(
            l10n("Topic") .. ": \"" .. phrase.props.text .. "\"", nil, { topicData = { topic = string.lower(phrase.props.text) } })
        table.insert(currentLine, topicPhrase)
    else
        applyMouseMoveEventToWidget(phrase, onJournalPageContentMouseMove)
        table.insert(currentLine, phrase)
    end
end

PT = {}

function PT.createPagesForJournal(splitPhrases, onTopicLinkClickCallback, updateJournalWindow, onJournalPageContentMouseMove)
    local pages = {}
    local linesPerPageLimit = math.floor(
        (window_sizer.calculateJournalWindowSize().y * constants.pageHeightFactorRelativeToJournalTexture) / settings.textJournalNormalSize())

    local lines = {}
    local lineWidthLimit = (window_sizer.calculateJournalWindowSize().x * constants.pageWidthFactorRelativeToJournalTexture)

    local linesGeneratedSoFar = 0
    local currentLine = {}
    local lineWidthSoFar = 0
    local encounteredWhitespaces = {}
    for _, phrase in ipairs(splitPhrases) do
        if phrase.userData.type == PhraseType.DOUBLE_NEWLINE then
            table.insert(lines, makeLineWidget(currentLine))
            currentLine = {}
            local wouldTwoMoreLinesFitIntoThePage = linesGeneratedSoFar + 2 < linesPerPageLimit
            if wouldTwoMoreLinesFitIntoThePage then
                insertPhraseIntoLine(currentLine, phrase, onTopicLinkClickCallback, updateJournalWindow, onJournalPageContentMouseMove)
                table.insert(lines, makeLineWidget(currentLine))
                currentLine = {}
                lineWidthSoFar = 0
                linesGeneratedSoFar = linesGeneratedSoFar + 2
            else
                table.insert(pages, ui.content(lines))
                lines = {}
                linesGeneratedSoFar = 0
            end
        elseif phrase.userData.type == PhraseType.NEWLINE then
            local wouldNextLineFitIntoThePage = linesGeneratedSoFar + 1 < linesPerPageLimit
            if wouldNextLineFitIntoThePage then
                table.insert(lines, makeLineWidget(currentLine))
                currentLine = {}
                lineWidthSoFar = 0
                linesGeneratedSoFar = linesGeneratedSoFar + 1
            else
                table.insert(pages, ui.content(lines))
                lines = {}
                linesGeneratedSoFar = 0
            end
        elseif phrase.userData.type == PhraseType.HEADER then
            local wouldNextLineFitIntoThePage = linesGeneratedSoFar + 1 < linesPerPageLimit
            if not wouldNextLineFitIntoThePage then
                table.insert(pages, ui.content(lines))
                lines = {}
                linesGeneratedSoFar = 0
            end
            insertPhraseIntoLine(currentLine, phrase, onTopicLinkClickCallback, updateJournalWindow, onJournalPageContentMouseMove)
            lineWidthSoFar = lineWidthSoFar + phrase.userData.width
        elseif phrase.userData.type == PhraseType.WHITESPACE then
            table.insert(encounteredWhitespaces, phrase)
        elseif phrase.userData.type == PhraseType.PUNCTUATION and #encounteredWhitespaces == 0 then
            insertPhraseIntoLine(currentLine, phrase, onTopicLinkClickCallback, updateJournalWindow, onJournalPageContentMouseMove)
            lineWidthSoFar = lineWidthSoFar + 0 + phrase.userData.width
        else
            local widthOfWhitespacesBeforeThisPhrase = 0
            for _, whitespace in pairs(encounteredWhitespaces) do
                widthOfWhitespacesBeforeThisPhrase = widthOfWhitespacesBeforeThisPhrase + whitespace.userData.width
            end
            local wouldThisPhraseFitIntoTheLine = (lineWidthSoFar + widthOfWhitespacesBeforeThisPhrase + phrase.userData.width) < lineWidthLimit
            if wouldThisPhraseFitIntoTheLine then
                for _, whitespace in pairs(encounteredWhitespaces) do
                    table.insert(currentLine, whitespace)
                end
                insertPhraseIntoLine(currentLine, phrase, onTopicLinkClickCallback, updateJournalWindow, onJournalPageContentMouseMove)
                lineWidthSoFar = lineWidthSoFar + widthOfWhitespacesBeforeThisPhrase + phrase.userData.width
            else
                local wouldNextLineFitIntoThePage = linesGeneratedSoFar + 1 < linesPerPageLimit
                if wouldNextLineFitIntoThePage then
                    table.insert(lines, makeLineWidget(currentLine))
                    currentLine = {}
                    lineWidthSoFar = 0
                    linesGeneratedSoFar = linesGeneratedSoFar + 1
                else
                    table.insert(lines, makeLineWidget(currentLine))
                    currentLine = {}
                    lineWidthSoFar = 0
                    table.insert(pages, ui.content(lines))
                    lines = {}
                    linesGeneratedSoFar = 0
                end
                insertPhraseIntoLine(currentLine, phrase, onTopicLinkClickCallback, updateJournalWindow, onJournalPageContentMouseMove)
                lineWidthSoFar = lineWidthSoFar + phrase.userData.width
            end
            encounteredWhitespaces = {}
        end
    end
    if #currentLine > 0 then
        table.insert(lines, makeLineWidget(currentLine))
    end
    if #lines > 0 then
        table.insert(pages, ui.content(lines))
    end

    return pages
end

return PT