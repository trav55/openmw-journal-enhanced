local ui = require('openmw.ui')
local util = require('openmw.util')
local l10n = require('openmw.core').l10n("openmw_journal_enhanced")
local templates = require("scripts.openmw_journal_enhanced.ui_layout.ui_templates")
local window_sizer = require('scripts.openmw_journal_enhanced.ui_layout.window_sizer')
local text_arrangement = require('scripts.openmw_journal_enhanced.window.page_text_arrangement')
local page_content_manipulation = require('scripts.openmw_journal_enhanced.window.page_content_manipulation')
local journal_callbacks = require('scripts.openmw_journal_enhanced.window.journal_callbacks')
local journal_button = require('scripts.openmw_journal_enhanced.window.journal_button')
local bookmark = require('scripts.openmw_journal_enhanced.window.bookmark')
local content_idx = require("scripts.openmw_journal_enhanced.window.content_indices")
local JournalUsed = require("scripts.openmw_journal_enhanced.window.journal_used")
local gamedata_entries = require("scripts.openmw_journal_enhanced.gamedata.journal_entries")
local journal_entry_content = require('scripts.openmw_journal_enhanced.wording.journal_entry_content')
local settings = require("scripts.openmw_journal_enhanced.settings")

local function createLeftPageTextBox()
    return {
        template = templates.leftJournalPage
    }
end

local function createRightPageTextBox()
    return {
        template = templates.rightJournalPage,
        props = {
            visible = true
        }
    }
end

local function createOptionsButton(callbacks)
    local result = journal_button.createJournalButton(callbacks.toggleBookmark, callbacks.updateJournalWindow)
    result.props = {
        relativePosition = util.vector2(
            templates.leftPageNumberText.props.relativePosition.x - 0.1,
            templates.leftPageNumberText.props.relativePosition.y),
        text = l10n("Options")
    }
    return result
end

local function createPrevButton(callbacks)
    local result = journal_button.createJournalButton(callbacks.changePageToPrevious, callbacks.updateJournalWindow)
    result.props = {
        relativePosition = util.vector2(
            templates.leftPageNumberText.props.relativePosition.x + 0.1,
            templates.leftPageNumberText.props.relativePosition.y),
        text = l10n("Prev")
    }
    return result
end

local function createNextButton(callbacks)
    local result = journal_button.createJournalButton(callbacks.changePageToNext, callbacks.updateJournalWindow)
    result.props = {
        relativePosition = util.vector2(
            templates.rightPageNumberText.props.relativePosition.x - 0.1,
            templates.rightPageNumberText.props.relativePosition.y),
        text = l10n("Next")
    }
    return result
end

local function createCloseButton(callbacks)
    local result = journal_button.createJournalButton(callbacks.closeJournal, callbacks.updateJournalWindow)
    result.props = {
        relativePosition = util.vector2(
            templates.rightPageNumberText.props.relativePosition.x + 0.1,
            templates.rightPageNumberText.props.relativePosition.y),
        text = l10n("Close")
    }
    return result
end

local function createReturnToJournalButton(callbacks)
    local result = journal_button.createJournalButton(callbacks.returnToJournal, callbacks.updateJournalWindow)
    result.props = {
        visible = false,
        relativePosition = util.vector2(
            templates.rightPageNumberText.props.relativePosition.x + 0.1,
            templates.rightPageNumberText.props.relativePosition.y),
        text = l10n("Journal")
    }
    return result
end

local function createPageNumberLeft(pageNumberText)
    return {
        template = templates.leftPageNumberText,
        props = {
            text = pageNumberText
        }
    }
end

local function createPageNumberRight(pageNumberText)
    return {
        template = templates.rightPageNumberText,
        props = {
            text = pageNumberText
        }
    }
end

local function createJournalWindowContents(journalWindowUserData)
    return {
        [content_idx.JOURNAL.leftPage] = createLeftPageTextBox(),
        [content_idx.JOURNAL.rightPage] = createRightPageTextBox(),
        [content_idx.JOURNAL.leftPageNumber] = createPageNumberLeft("654"),
        [content_idx.JOURNAL.rightPageNumber] = createPageNumberRight("655"),
        [content_idx.JOURNAL.optionsButton] = createOptionsButton(journalWindowUserData.callbacks),
        [content_idx.JOURNAL.prevButton] = createPrevButton(journalWindowUserData.callbacks),
        [content_idx.JOURNAL.nextButton] = createNextButton(journalWindowUserData.callbacks),
        [content_idx.JOURNAL.closeButton] = createCloseButton(journalWindowUserData.callbacks),
        [content_idx.JOURNAL.journalBookmark] = bookmark.createJournalBookmark(journalWindowUserData),
        [content_idx.JOURNAL.returnToJournalButton] = createReturnToJournalButton(journalWindowUserData.callbacks)
    }
end

local J = {}

function J.createJournalWindow(nonEngineData)
    local journalWindow = {
        layer = 'Windows',
        type = ui.TYPE.Image,
        props = {
            size = window_sizer.calculateJournalWindowSize(),
            relativePosition = util.vector2(0.5, 0.5),
            anchor = util.vector2(0.5, 0.5),
            resource = ui.texture { path = 'textures/Tx_menubook.dds' }, --_REFC
            visible = false
        },
        userData = {
            gameData = gamedata_entries.createDatabase(nonEngineData),
            currentJournalPage = 1,
            usedJournal = JournalUsed.Overall,
            journalTextArranged = {
                [JournalUsed.Overall] = {},
                [JournalUsed.Topic] = {},
                [JournalUsed.SingleQuest] = {},
            },
            hashedWordWidths = {
                __journalTextFontSizeThisTableIsBasedOn = settings.textJournalNormalSize()
            }
        }
    }

    local uiVal = ui.create(journalWindow)

    journal_callbacks.addCallbacks(uiVal)

    uiVal.layout.content = ui.content(createJournalWindowContents(uiVal.layout.userData))

    uiVal.layout.userData.journalTextArranged[uiVal.layout.userData.usedJournal] =
        text_arrangement.createPagesForJournal(
            journal_entry_content.generateJournalEntriesSplitIntoPhrases(
                uiVal.layout.userData.gameData,
                uiVal.layout.userData.hashedWordWidths,
                uiVal.layout.userData.callbacks.rightClickCreateContextMenu),
            uiVal.layout.userData.callbacks.changeJournalToSpecificTopicView,
            uiVal.layout.userData.callbacks.updateJournalWindow,
            uiVal.layout.userData.callbacks.onJournalPageContentMouseMove)
    page_content_manipulation.initializeJournalPages(uiVal.layout)

    return uiVal
end

function J.updateJournalWindowOnOpen(journalWindow, nonEngineData)
    journalWindow.userData.gameData.updateDatabase(nonEngineData)
    journalWindow.props.size = window_sizer.calculateJournalWindowSize()

    local currentFontSize = settings.textJournalNormalSize()
    if journalWindow.userData.hashedWordWidths.__journalTextFontSizeThisTableIsBasedOn ~= currentFontSize then
        journalWindow.userData.hashedWordWidths = {
            __journalTextFontSizeThisTableIsBasedOn = currentFontSize
        }
    end

    journalWindow.userData.usedJournal = JournalUsed.Overall
    journalWindow.userData.journalTextArranged[journalWindow.userData.usedJournal] =
        text_arrangement.createPagesForJournal(
            journal_entry_content.generateJournalEntriesSplitIntoPhrases(
                journalWindow.userData.gameData,
                journalWindow.userData.hashedWordWidths,
                journalWindow.userData.callbacks.rightClickCreateContextMenu),
            journalWindow.userData.callbacks.changeJournalToSpecificTopicView,
            journalWindow.userData.callbacks.updateJournalWindow,
            journalWindow.userData.callbacks.onJournalPageContentMouseMove)
    page_content_manipulation.initializeJournalPages(journalWindow)
end

return J
