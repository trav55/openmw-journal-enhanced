local content_idx = require("scripts.openmw_journal_enhanced.window.content_indices")
local quest_stage_editing = require("scripts.openmw_journal_enhanced.outside_manipulators.quest_stage_editing")

local MW = {}

function MW.manipulateJournalBasedOnMouseWheelTurn(journalWindow, x)
    if quest_stage_editing.handleMouseWheelEvent(x) then
        return
    end

    if journalWindow.layout.content[content_idx.JOURNAL.journalBookmark].props.visible then
        journalWindow.layout.content[content_idx.JOURNAL.journalBookmark].userData.callbacks.onMouseWheel(x)
        return
    end

    if x > 0 then     --up
        journalWindow.layout.userData.callbacks.changePageToPrevious()
    elseif x < 0 then -- down
        journalWindow.layout.userData.callbacks.changePageToNext()
    end
end

return MW
