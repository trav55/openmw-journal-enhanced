local ui = require('openmw.ui')
local constants = require("scripts.openmw_journal_enhanced.ui_layout.ui_constants")
local util = require('openmw.util')
local templates = require("scripts.openmw_journal_enhanced.ui_layout.ui_templates")
local window_sizer = require('scripts.openmw_journal_enhanced.ui_layout.window_sizer')
local ui_text = require("scripts.openmw_journal_enhanced.ui_layout.ui_text")
local pairsByKeys = require("scripts.openmw_journal_enhanced.utils.pairs_by_keys").pairsByKeys

local function createAlphabetColumnSeparator()
    return {
        type = ui.TYPE.Image,
        external = {
            grow = 0.2
        },
        props = {
            resource = constants.whiteTexture,
            size = util.vector2(1, 1),
            alpha = 0.0
        },
    }
end

local ENGLISH_ALPHABET = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P",
    "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"}

local function createLettersForAlphabet(gameData)
    local lettersAndIfThereIsATopicUsingThem = {}
    for _, basicLetter in pairs(ENGLISH_ALPHABET) do
        lettersAndIfThereIsATopicUsingThem[basicLetter] = false
    end

    for firstTopicCharByte, topicIndicesForThisCharacter in pairs(gameData.topicsIdxsByTheirFirstCharByte) do
        lettersAndIfThereIsATopicUsingThem[string.upper(string.char(firstTopicCharByte))] = (#topicIndicesForThisCharacter > 0)
    end

    return lettersAndIfThereIsATopicUsingThem
end

local function createAlphabetColumns(gameData, onLetterClickCallback, updateJournalWindow)
    local alphabetLettersAndUsage = createLettersForAlphabet(gameData)
    local numberOfLettersInOneColumn = 0
    for _ in pairs(alphabetLettersAndUsage) do numberOfLettersInOneColumn = numberOfLettersInOneColumn + 1 end

    local lineLimit = window_sizer.calculateNumberOfVisibleBookmarkAlphabetLines()
    while numberOfLettersInOneColumn > lineLimit do
        numberOfLettersInOneColumn = math.ceil(numberOfLettersInOneColumn / 2)
    end

    local widgets = {}
    local thisLetterColumn = {}
    local lettersAccountedFor = 0
    for letter, isUsed in pairsByKeys(alphabetLettersAndUsage) do
        if lettersAccountedFor % numberOfLettersInOneColumn == 0 and #thisLetterColumn > 0 then
            table.insert(widgets, createAlphabetColumnSeparator())
            table.insert(widgets, {
                template = templates.alphabetColumn,
                content = ui.content(thisLetterColumn)
            })
            thisLetterColumn = {}
        end
        table.insert(thisLetterColumn, ui_text.createAlphabetTextWidget(letter, isUsed, onLetterClickCallback, updateJournalWindow))
        lettersAccountedFor = lettersAccountedFor + 1
    end
    if #thisLetterColumn > 0 then
        table.insert(widgets, createAlphabetColumnSeparator())
        table.insert(widgets, {
            template = templates.alphabetColumn,
            content = ui.content(thisLetterColumn)
        })
    end
    table.insert(widgets, createAlphabetColumnSeparator())

    return widgets
end

local A = {}

function A.updateAlphabetContent(alphabetLayout, gameData, onLetterClickCallback, updateJournalWindow)
    alphabetLayout.content = ui.content(createAlphabetColumns(gameData, onLetterClickCallback, updateJournalWindow))
end

function A.createTopicAlphabet()
    local result = {
        type = ui.TYPE.Flex,
        props = {
            visible = true,
            horizontal = true,
            autoSize = false,
            align = ui.ALIGNMENT.Center,
            size = util.vector2(0, 0),
            relativePosition = util.vector2(0.0225, 0.085),
            relativeSize = util.vector2(0.595, constants.bookmarkScrollableAreaHeightFactorRelativeToBookmarkHeight),
        },
    }
    return result
end

return A