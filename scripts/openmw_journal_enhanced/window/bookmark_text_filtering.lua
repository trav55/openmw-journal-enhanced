
local async = require('openmw.async')
local ui = require('openmw.ui')
local util = require('openmw.util')
local l10n = require('openmw.core').l10n("openmw_journal_enhanced")
local templates = require("scripts.openmw_journal_enhanced.ui_layout.ui_templates")
local constants = require("scripts.openmw_journal_enhanced.ui_layout.ui_constants")

local TF = {}

function TF.resetFilteringInput(filteringInput)
    filteringInput.content.inputHelperText.props.visible = true
    filteringInput.content.inputText.props.text = ""
end

function TF.createFilteringInput(callbacks, updateJournalWindow)
    local filterInputTextBox = {
        name = "inputText",
        template = templates.filteringInputTextBox,
        props = {
            text = "",
            multiline = false,
            relativeSize = util.vector2(1, 1),
            autoSize = true,
        }
    }

    local result = {
        type = ui.TYPE.Image,
        props = {
            size = util.vector2(constants.textJournalInputSize*10, constants.textJournalInputSize),
            anchor = util.vector2(0.5, 0.5),
            resource = ui.texture{ path = 'white' },
            color = constants.paperLikeColor,
            alpha = 0.8,
        },
        content = ui.content({
            {
                name = "inputHelperText",
                type = ui.TYPE.Text,
                props = {
                    textSize = constants.textJournalInputSize,
                    textColor = constants.fontColorInactiveIdle,
                    text = l10n("WriteFiltersHere")
                }
            },
            filterInputTextBox,
            {
                name = "underLine",
                template = templates.journalButtonUnderline,
                props = {
                    visible = false
                },
            }
        }),
    }

    filterInputTextBox.events =
    {
        textChanged = async:callback(function(text)
            result.content.inputHelperText.props.visible = (text == "")
            filterInputTextBox.props.text = text
            callbacks.applyTextFilter(text)
            updateJournalWindow()
        end),
        focusGain = async:callback(function ()
            result.content.underLine.props.visible = true
            result.props.color = constants.paperLikeColorLighter
            updateJournalWindow()
        end),
        focusLoss = async:callback(function ()
            result.content.underLine.props.visible = false
            result.props.color = constants.paperLikeColor
            updateJournalWindow()
        end)
    }

    return result
end

return TF