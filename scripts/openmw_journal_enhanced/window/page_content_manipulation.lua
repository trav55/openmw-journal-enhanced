local ui = require('openmw.ui')
local templates = require("scripts.openmw_journal_enhanced.ui_layout.ui_templates")
local settings = require("scripts.openmw_journal_enhanced.settings")
local content_idx = require("scripts.openmw_journal_enhanced.window.content_indices")

local function setPageContent(journalWindow, pageWidgetIdx, journalPageNumber)
    if journalWindow.userData.journalTextArranged[journalWindow.userData.usedJournal][journalPageNumber] then
        journalWindow.content[pageWidgetIdx].content = ui.content({
            {
                template = templates.journalPageText,
                content = ui.content( journalWindow.userData.journalTextArranged[journalWindow.userData.usedJournal][journalPageNumber] )
            }
        })
    else
        journalWindow.content[pageWidgetIdx].content = ui.content({
            {
                template = templates.journalPageText,
                content = ui.content( {} )
            }
        })
    end
end

local PM = {}

function PM.resetCurrentPageViewed(journalWindow, toFirstPage)
    if toFirstPage then
        journalWindow.userData.currentJournalPage = 1
        return
    end
    journalWindow.userData.currentJournalPage = math.max(1, #journalWindow.userData.journalTextArranged[journalWindow.userData.usedJournal])
    if journalWindow.userData.currentJournalPage % 2 == 0 then
        journalWindow.userData.currentJournalPage = journalWindow.userData.currentJournalPage-1
    end
end

function PM.setJournalPages(journalWindow)
    setPageContent(journalWindow, content_idx.JOURNAL.leftPage, journalWindow.userData.currentJournalPage)
    setPageContent(journalWindow, content_idx.JOURNAL.rightPage, journalWindow.userData.currentJournalPage+1)

    journalWindow.content[content_idx.JOURNAL.leftPageNumber].props.text = tostring(journalWindow.userData.currentJournalPage)
    journalWindow.content[content_idx.JOURNAL.rightPageNumber].props.text = tostring(journalWindow.userData.currentJournalPage+1)

    journalWindow.content[content_idx.JOURNAL.prevButton].props.visible = (journalWindow.userData.currentJournalPage > 1)
    journalWindow.content[content_idx.JOURNAL.nextButton].props.visible =
        (journalWindow.userData.currentJournalPage < #journalWindow.userData.journalTextArranged[journalWindow.userData.usedJournal]-1)
end

function PM.initializeJournalPages(journalWindow)
    PM.resetCurrentPageViewed(journalWindow)
    PM.setJournalPages(journalWindow)
    journalWindow.content[content_idx.JOURNAL.leftPageNumber].props.textSize = settings.textJournalPageNumberSize()
    journalWindow.content[content_idx.JOURNAL.rightPageNumber].props.textSize = settings.textJournalPageNumberSize()
    journalWindow.content[content_idx.JOURNAL.closeButton].props.visible = true
    journalWindow.content[content_idx.JOURNAL.returnToJournalButton].props.visible = false
    if journalWindow.content[content_idx.JOURNAL.journalBookmark].props.visible then
    -- if true then
    --     journalWindow.content[content_idx.JOURNAL.journalBookmark].props.visible = false
    --     journalWindow.content[content_idx.JOURNAL.rightPage].props.visible = true
        PM.toggleBookmark(journalWindow)
    end
end

function PM.toggleBookmark(journalWindow)
    journalWindow.content[content_idx.JOURNAL.journalBookmark].props.visible = not journalWindow.content[content_idx.JOURNAL.journalBookmark].props.visible
    journalWindow.content[content_idx.JOURNAL.rightPage].props.visible = not journalWindow.content[content_idx.JOURNAL.rightPage].props.visible
    if journalWindow.content[content_idx.JOURNAL.journalBookmark].props.visible then
        journalWindow.content[content_idx.JOURNAL.journalBookmark].userData.callbacks.onBookmarkUpdate(journalWindow.userData.gameData)
    end
end

return PM