local page_content_manipulation = require('scripts.openmw_journal_enhanced.window.page_content_manipulation')
local ambient = require('openmw.ambient')
local content_idx = require("scripts.openmw_journal_enhanced.window.content_indices")
local journal_entry_content = require('scripts.openmw_journal_enhanced.wording.journal_entry_content')
local text_arrangement = require('scripts.openmw_journal_enhanced.window.page_text_arrangement')
local tooltip = require("scripts.openmw_journal_enhanced.outside_manipulators.tooltip")
local context_menu = require("scripts.openmw_journal_enhanced.outside_manipulators.context_menu")
local JournalUsed = require("scripts.openmw_journal_enhanced.window.journal_used")
local I = require('openmw.interfaces')

local setViewToFirstPage = true

local function prepareTopicCallback(journalWindow)
    local function result(buttonCallingThisFunction)
        journalWindow.userData.usedJournal = JournalUsed.Topic
        journalWindow.userData.journalTextArranged[journalWindow.userData.usedJournal] =
            text_arrangement.createPagesForJournal(
                journal_entry_content.generateEntriesForTopic(
                    journalWindow.userData.gameData,
                    buttonCallingThisFunction.props.text,
                    journalWindow.userData.hashedWordWidths),
                journalWindow.userData.callbacks.changeJournalToSpecificTopicView,
                journalWindow.userData.callbacks.updateJournalWindow,
                journalWindow.userData.callbacks.onJournalPageContentMouseMove)
        page_content_manipulation.resetCurrentPageViewed(journalWindow, setViewToFirstPage)
        page_content_manipulation.setJournalPages(journalWindow)
        journalWindow.content[content_idx.JOURNAL.closeButton].props.visible = false
        journalWindow.content[content_idx.JOURNAL.returnToJournalButton].props.visible = true
        ambient.playSound("book page")
    end

    return result
end

local function prepareContextMenuCreateCallback(journalWindow)
    local function result(mouseEvent, objectOnWhichRightClickWasMade)
        context_menu.createContextMenu(mouseEvent, objectOnWhichRightClickWasMade, journalWindow)
    end

    return result
end

local function prepareSingleQuestCallback(journalWindow)
    local function result(buttonCallingThisFunction)
        journalWindow.userData.usedJournal = JournalUsed.SingleQuest
        journalWindow.userData.journalTextArranged[journalWindow.userData.usedJournal] =
            text_arrangement.createPagesForJournal(
                journal_entry_content.generateEntriesForSingleQuest(
                    journalWindow.userData.gameData,
                    buttonCallingThisFunction.userData.questId,
                    journalWindow.userData.hashedWordWidths,
                    journalWindow.userData.callbacks.rightClickCreateContextMenu),
                prepareTopicCallback(journalWindow),
                journalWindow.userData.callbacks.updateJournalWindow,
                journalWindow.userData.callbacks.onJournalPageContentMouseMove)
        page_content_manipulation.resetCurrentPageViewed(journalWindow, setViewToFirstPage)
        page_content_manipulation.setJournalPages(journalWindow)
        journalWindow.content[content_idx.JOURNAL.closeButton].props.visible = false
        journalWindow.content[content_idx.JOURNAL.returnToJournalButton].props.visible = true
        ambient.playSound("book page")
    end

    return result
end

local PC = {}

function PC.addCallbacks(journalWindow)
    local function dummyCallback()
        print("dummy")
    end
    local function updateJournalWindow()
        tooltip.killTooltip()
        journalWindow:update()
    end
    local function changePageToPrevious()
        local newPageCandidate = math.max(1, journalWindow.layout.userData.currentJournalPage - 2)
        if newPageCandidate ~= journalWindow.layout.userData.currentJournalPage then
            journalWindow.layout.userData.currentJournalPage = newPageCandidate
            page_content_manipulation.setJournalPages(journalWindow.layout)
            ambient.playSound("book page")
        end
    end
    local function changePageToNext()
        local numberOfPages =
            math.max(1, #journalWindow.layout.userData.journalTextArranged[journalWindow.layout.userData.usedJournal])
        if numberOfPages % 2 == 0 then
            numberOfPages = numberOfPages - 1
        end
        local newPageCandidate = math.min(numberOfPages, journalWindow.layout.userData.currentJournalPage + 2)

        if newPageCandidate ~= journalWindow.layout.userData.currentJournalPage then
            journalWindow.layout.userData.currentJournalPage = newPageCandidate
            page_content_manipulation.setJournalPages(journalWindow.layout)
            ambient.playSound("book page2")
        end
    end
    local function closeJournal()
        I.UI.removeMode("Journal")
    end
    local function toggleBookmark()
        page_content_manipulation.toggleBookmark(journalWindow.layout)
        ambient.playSound("book page")
    end
    local function returnToJournal()
        journalWindow.layout.userData.usedJournal = JournalUsed.Overall
        page_content_manipulation.resetCurrentPageViewed(journalWindow.layout)
        page_content_manipulation.setJournalPages(journalWindow.layout)
        journalWindow.layout.content[content_idx.JOURNAL.closeButton].props.visible = true
        journalWindow.layout.content[content_idx.JOURNAL.returnToJournalButton].props.visible = false
        ambient.playSound("book page")
    end
    journalWindow.layout.userData.callbacks = {
        dummy = dummyCallback,
        updateJournalWindow = updateJournalWindow,
        changePageToPrevious = changePageToPrevious,
        changePageToNext = changePageToNext,
        closeJournal = closeJournal,
        toggleBookmark = toggleBookmark,
        changeJournalToSpecificTopicView = prepareTopicCallback(journalWindow.layout),
        changeJournalToSingleQuestView = prepareSingleQuestCallback(journalWindow.layout),
        returnToJournal = returnToJournal,
        onJournalPageContentMouseMove = tooltip.createMouseMoveCallback(journalWindow.layout.userData.gameData),
        rightClickCreateContextMenu = prepareContextMenuCreateCallback(journalWindow.layout),
    }
end

return PC
