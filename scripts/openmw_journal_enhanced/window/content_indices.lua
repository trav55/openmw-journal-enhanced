local IDX = {
    JOURNAL = {},
    BOOKMARK = {}
}

IDX.JOURNAL.leftPage = 1
IDX.JOURNAL.rightPage = 2
IDX.JOURNAL.leftPageNumber = 3
IDX.JOURNAL.rightPageNumber = 4
IDX.JOURNAL.optionsButton = 5
IDX.JOURNAL.prevButton = 6
IDX.JOURNAL.nextButton = 7
IDX.JOURNAL.closeButton = 8
IDX.JOURNAL.journalBookmark = 9
IDX.JOURNAL.returnToJournalButton = 10

IDX.BOOKMARK.alphabet = 1
IDX.BOOKMARK.scrollableSpace = 2
IDX.BOOKMARK.filteringInput = 3
IDX.BOOKMARK.questFilteringButton = 4
IDX.BOOKMARK.topicsButton = 5
IDX.BOOKMARK.cancelButton = 6
IDX.BOOKMARK.questsButton = 7

return IDX