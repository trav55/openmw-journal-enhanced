local ui = require('openmw.ui')
local templates = require("scripts.openmw_journal_enhanced.ui_layout.ui_templates")
local async = require('openmw.async')

local CL = {}

local LEFT_MOUSE_BUTTON = 1
local RIGHT_MOUSE_BUTTON = 3

function CL.createFocusableWidget(updateJournalWindow)
    return {
        userData = {
            isFocused = false,
            rightClickCallback = nil,
        },
        events = {
            focusGain = async:callback(function(e, thisObject)
                if thisObject.userData.textColorOver ~= nil then
                    thisObject.props.textColor = thisObject.userData.textColorOver
                end
                thisObject.userData.isFocused = true
                if updateJournalWindow ~= nil then
                    updateJournalWindow()
                end
            end),
            focusLoss = async:callback(function(e, thisObject)
                if thisObject.userData.textColorIdle ~= nil then
                    thisObject.props.textColor = thisObject.userData.textColorIdle
                end
                thisObject.userData.isFocused = false
                if (updateJournalWindow ~= nil) and (thisObject.userData.isFocused) and (not thisObject.userData.isPressed) then
                    updateJournalWindow()
                end
            end),
            mouseRelease = async:callback(function(mouseEvent, thisObject)
                if mouseEvent.button == RIGHT_MOUSE_BUTTON then
                    if thisObject.userData.rightClickCallback ~= nil then
                        thisObject.userData.rightClickCallback(mouseEvent, thisObject)
                    end
                end
            end),
        },
        content = ui.content(
            {
                {
                    template = templates.journalButtonUnderline,
                    props = {
                        visible = false
                    }
                }
            })
    }
end

function CL.createClickableWidget(onClickCallback, updateJournalWindow)
    local result = CL.createFocusableWidget(updateJournalWindow)
    result.userData.isPressed = false
    result.userData.onClicking = onClickCallback
    result.events.mousePress = async:callback(function(mouseEvent, thisObject)
        if mouseEvent.button ~= LEFT_MOUSE_BUTTON then
            return
        end
        thisObject.props.textColor = thisObject.userData.textColorPressed
        thisObject.userData.isPressed = true
        if updateJournalWindow ~= nil then
            updateJournalWindow()
        end
    end)
    result.events.mouseRelease = async:callback(function(mouseEvent, thisObject)
        if mouseEvent.button == RIGHT_MOUSE_BUTTON then
            if thisObject.userData.rightClickCallback ~= nil then
                thisObject.userData.rightClickCallback(mouseEvent, thisObject)
            end
            return
        elseif mouseEvent.button ~= LEFT_MOUSE_BUTTON then
            return
        end
        if thisObject.userData.isFocused then
            if thisObject.userData.textColorOver ~= nil then
                thisObject.props.textColor = thisObject.userData.textColorOver
            end
        else
            if thisObject.userData.textColorIdle ~= nil then
                thisObject.props.textColor = thisObject.userData.textColorIdle
            end
        end
        thisObject.userData.isPressed = false
        thisObject.userData.onClicking(thisObject)
        if updateJournalWindow ~= nil then
            updateJournalWindow()
        end
    end)
    return result
end

return CL
