local constants = require("scripts.openmw_journal_enhanced.ui_layout.ui_constants")
local ui = require('openmw.ui')
local util = require('openmw.util')
local auxUi = require('openmw_aux.ui')

local T = {
    journalPage = {
        type = ui.TYPE.Image,
        props = {
            size = util.vector2(0, 0),
            relativeSize = util.vector2(
                constants.pageWidthFactorRelativeToJournalTexture,
                constants.pageHeightFactorRelativeToJournalTexture),
            -- resource = constants.whiteTexture,
            -- color = constants.paperLikeColor
        }
    },
    journalPageText = {
        type = ui.TYPE.Flex,
        props = {
            size = util.vector2(0, 0),
            horizontal = false,
            relativeSize = util.vector2(1.0, 1.0)
        }
    },
    journalTextNormal = {
        type = ui.TYPE.Text,
        props = {
            textSize = constants.textJournalNormalSize,
            textColor = constants.fontColorJournalNormalText
        }
    },
    journalPageNumberText = {
        type = ui.TYPE.Text,
        props = {
            multiline = false,
            wordWrap = false,
            autoSize = true,
            anchor = util.vector2(0.5, 0.5),
            textAlignH = ui.ALIGNMENT.Center,
            textAlignV = ui.ALIGNMENT.Center,
            textSize = constants.textJournalPageNumberSize,
            textColor = constants.textJournalPageNumberColor
        }
    },
    journalTextButtonIdle = {
        type = ui.TYPE.Text,
        props = {
            multiline = false,
            wordWrap = false,
            autoSize = true,
            anchor = util.vector2(0.5, 0.5),
            textAlignH = ui.ALIGNMENT.Center,
            textAlignV = ui.ALIGNMENT.Center,
            textSize = constants.textJournalButtonSize,
            textColor = constants.fontColorJournalButtonIdle,
            textShadow = true,
            textShadowColor = constants.fontColorJournalButtonShadow,
        }
    },
    journalButtonUnderline = {
        type = ui.TYPE.Image,
        props = {
            size = util.vector2(0, 1),
            relativeSize = util.vector2(1, 0),
            relativePosition = util.vector2(0, 0.9),
            resource = constants.whiteTexture,
            color = constants.fontColorJournalButtonIdle
        }
    },
    journalBookmark = {
        type = ui.TYPE.Image,
        props = {
            relativeSize = util.vector2(0.555, constants.bookmarkHeightFactorRelativeToJournalWindowHeight),
            relativePosition = util.vector2(0.51, 0),
            resource = ui.texture { path = 'textures/Tx_menubook_bookmark.dds' },
            visible = true
        }
    },
    alphabetColumn = {
        type = ui.TYPE.Flex,
        props = {
            horizontal = false,
            autoSize = true,
            align = ui.ALIGNMENT.Start,
        }
    },
    filteringInputTextBox = {
        type = ui.TYPE.TextEdit,
        props = {
            autoSize = true,
            textSize = constants.textJournalInputSize,
            textColor = constants.fontColorJournalNormalText,
            multiline = false,
        }
    },
}

T.journalRawTextBox = auxUi.deepLayoutCopy(T.journalTextNormal)
T.journalRawTextBox.props.multiline = true
T.journalRawTextBox.props.wordWrap = true
T.journalRawTextBox.props.autoSize = false
T.journalRawTextBox.props.relativeSize = util.vector2(1.0, 1.0)

local shiftFactorForPageNumberToAccountForMargins = 0.008
T.leftPageNumberText = auxUi.deepLayoutCopy(T.journalPageNumberText)
T.leftPageNumberText.props.relativePosition = util.vector2(
    constants.leftPageTopLeftWCornerFactorRelativeToJournalTexture
    + (constants.pageWidthFactorRelativeToJournalTexture / 2) - shiftFactorForPageNumberToAccountForMargins,
    constants.pageHeightFactorRelativeToJournalTexture + 0.075)
T.rightPageNumberText = auxUi.deepLayoutCopy(T.journalPageNumberText)
T.rightPageNumberText.props.relativePosition = util.vector2(
    constants.rightPageTopRightWCornerFactorRelativeToJournalTextureTopRight
    - (constants.pageWidthFactorRelativeToJournalTexture / 2) + shiftFactorForPageNumberToAccountForMargins,
    constants.pageHeightFactorRelativeToJournalTexture + 0.075)

T.leftJournalPage = auxUi.deepLayoutCopy(T.journalPage)
T.leftJournalPage.props.relativePosition = util.vector2(
    constants.leftPageTopLeftWCornerFactorRelativeToJournalTexture,
    constants.leftPageTopLeftHCornerFactorRelativeToJournalTexture)

T.rightJournalPage = auxUi.deepLayoutCopy(T.journalPage)
T.rightJournalPage.props.anchor = util.vector2(1.0, 0.0)
T.rightJournalPage.props.relativePosition = util.vector2(
    constants.rightPageTopRightWCornerFactorRelativeToJournalTextureTopRight,
    constants.leftPageTopLeftHCornerFactorRelativeToJournalTexture)

T.journalTextLinkIdle = auxUi.deepLayoutCopy(T.journalTextNormal)
T.journalTextLinkIdle.props.textColor = constants.fontColorJournalLinkIdle

T.journalTextHeader = auxUi.deepLayoutCopy(T.journalTextNormal)
T.journalTextHeader.props.textColor = constants.fontColorJournalEntryHeader

T.journalAlphabetLinkText = auxUi.deepLayoutCopy(T.journalTextNormal)
T.journalAlphabetLinkText.props.anchor = util.vector2(0.5, 0.5)
T.journalAlphabetLinkText.props.textSize = constants.textBookmarkLinkSize

T.journalAlphabetLinkTextUnused = auxUi.deepLayoutCopy(T.journalAlphabetLinkText)
T.journalAlphabetLinkTextUnused.props.textColor = constants.fontColorInactiveIdle

T.contextMenuTextLinkIdle = auxUi.deepLayoutCopy(T.journalTextNormal)
T.contextMenuTextLinkIdle.props.textSize = constants.textContextMenuOptionSize
T.contextMenuTextLinkIdle.props.textColor = constants.paperLikeColor

T.paperColoredUnderline = auxUi.deepLayoutCopy(T.journalButtonUnderline)
T.paperColoredUnderline.props.color = constants.paperLikeColorLighter

return T
