local openmwConstants = require('scripts.omw.mwui.constants')
local util = require('openmw.util')
local ui = require('openmw.ui')
local core = require('openmw.core')

local function getColorFromGameSettings(colorTag)
    local result = core.getGMST(colorTag)
    local rgb = {}
    for color in string.gmatch(result, '(%d+)') do
        table.insert(rgb, tonumber(color))
    end
    if #rgb ~= 3 then
        print("UNEXPECTED COLOR: rgb of size=", #rgb)
        return util.color.rgb(1, 1, 1)
    end
    return util.color.rgb(rgb[1] / 255, rgb[2] / 255, rgb[3] / 255)
end

local C = {
    updateWindowEveryXFrame = 20,
    journalTextureWidth = 705,
    journalTextureHeight = 390,
    journalWindowWidthMultiplier = 2.0,
    journalWindowHeightMultiplier = 2.0,
    bookmarkHeightFactorRelativeToJournalWindowHeight = 0.9,
    bookmarkScrollableAreaHeightFactorRelativeToBookmarkHeight = 0.63,
    leftPageTopLeftCornerWFromTopLeftCornerOfJournalTexture = 80,
    leftPageTopLeftCornerHFromTopLeftCornerOfJournalTexture = 10,
    bookmarkScrollButtonSize = 18,
    bookmarkScrollRatio = 0.08,
    textJournalNormalSize = openmwConstants.textNormalSize + 10,
    textJournalButtonSize = openmwConstants.textNormalSize + 11,
    textContextMenuOptionSize = openmwConstants.textNormalSize + 5,
    fontColorJournalNormalText = getColorFromGameSettings("FontColor_color_background"),
    fontColorJournalButtonIdle = util.color.rgb(60 / 255, 24 / 255, 4 / 255),
    fontColorJournalButtonOver = util.color.rgb(127 / 255, 52 / 255, 8 / 255),
    fontColorJournalButtonShadow = util.color.rgb(234 / 255, 210 / 255, 175 / 255),
    fontColorJournalButtonPressed = util.color.rgb(255 / 255, 255 / 255, 189 / 255),
    fontColorJournalEntryHeader = util.color.rgb(153 / 255, 0 / 255, 0 / 255),
    fontColorJournalLinkIdle = getColorFromGameSettings("FontColor_color_journal_link"),
    fontColorJournalLinkOver = getColorFromGameSettings("FontColor_color_journal_link_over"),
    fontColorJournalLinkPressed = getColorFromGameSettings("FontColor_color_journal_link_pressed"),
    fontColorInactiveIdle = util.color.rgb(51 / 255, 51 / 255, 51 / 255),
    fontColorInactiveOver = util.color.rgb(102 / 255, 102 / 255, 102 / 255),
    fontColorInactivePressed = util.color.rgb(127 / 255, 127 / 255, 127 / 255),
    fontColorActor = util.color.rgb(0 / 255, 51 / 255, 0 / 255),
    paperLikeColor = getColorFromGameSettings("FontColor_color_normal"),
    paperLikeColorLighter = getColorFromGameSettings("FontColor_color_normal_over"),
    paperLikeColorPressed = getColorFromGameSettings("FontColor_color_normal_pressed"),
    whiteTexture = ui.texture { path = 'white' },
    tooltipOffset = util.vector2(3, 20),
    contextMenuOffset = util.vector2(-10, -10),
}

C.textBookmarkAlphabetLetterSize = C.textJournalNormalSize * 1.15
C.textBookmarkLinkSize = C.textJournalNormalSize
C.textJournalInputSize = C.textJournalNormalSize
C.textTooltipSize = C.textJournalNormalSize

C.pageWidthRelevantToJournalTexture = C.journalTextureWidth * 0.311                        -- 159 for 512
C.pageHeightRelevantToJournalTexture = C.journalTextureHeight * 0.852                      -- 218 for 256
C.leftPageTopLeftCornerWFromTopLeftCornerOfJournalTexture = C.journalTextureWidth * 0.156  -- 80 for 512
C.leftPageTopLeftCornerHFromTopLeftCornerOfJournalTexture = C.journalTextureHeight * 0.039 -- 10 for 256

C.pageWidthFactorRelativeToJournalTexture = C.pageWidthRelevantToJournalTexture / C.journalTextureWidth
C.pageHeightFactorRelativeToJournalTexture = C.pageHeightRelevantToJournalTexture / C.journalTextureHeight

C.textJournalPageNumberSize = C.textJournalNormalSize
C.textJournalPageNumberColor = C.fontColorJournalNormalText

C.leftPageTopLeftWCornerFactorRelativeToJournalTexture =
    C.leftPageTopLeftCornerWFromTopLeftCornerOfJournalTexture / C.journalTextureWidth
C.leftPageTopLeftHCornerFactorRelativeToJournalTexture =
    C.leftPageTopLeftCornerHFromTopLeftCornerOfJournalTexture / C.journalTextureHeight

C.rightPageTopRightWCornerFactorRelativeToJournalTextureTopRight =
    (C.journalTextureWidth - C.leftPageTopLeftCornerWFromTopLeftCornerOfJournalTexture) / C.journalTextureWidth

return C
