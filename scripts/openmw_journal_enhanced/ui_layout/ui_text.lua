local templates = require("scripts.openmw_journal_enhanced.ui_layout.ui_templates")
local settings = require("scripts.openmw_journal_enhanced.settings")
local constants = require("scripts.openmw_journal_enhanced.ui_layout.ui_constants")
local ui_clickable = require("scripts.openmw_journal_enhanced.ui_layout.ui_clickable")

local TXT = {}

function TXT.createNormalTextWidget(text)
    return {
        template = templates.journalTextNormal,
        props = {
            text = text,
            textSize = settings.textJournalNormalSize()
        },
    }
end

function TXT.createHeaderTextWidget(text)
    local result = ui_clickable.createFocusableWidget()
    result.template = templates.journalTextHeader
    result.props = {
        text = text,
        textSize = settings.textJournalNormalSize()
    }
    return result
end

function TXT.createLinkTextWidget(text, onClickCallback, updateJournalWindow)
    local result = ui_clickable.createClickableWidget(onClickCallback, updateJournalWindow)
    result.template = templates.journalTextLinkIdle
    result.userData.textColorIdle = constants.fontColorJournalLinkIdle
    result.userData.textColorOver = constants.fontColorJournalLinkOver
    result.userData.textColorPressed = constants.fontColorJournalLinkPressed
    result.props = {
        text = text,
        textSize = settings.textJournalNormalSize()
    }
    return result
end

function TXT.createBookmarkLinkWidget(text, isActive, onClickCallback, updateJournalWindow, onJournalPageContentMouseMove)
    local result = ui_clickable.createClickableWidget(onClickCallback, updateJournalWindow)
    if isActive then
        result.template = templates.journalAlphabetLinkText
        result.userData.textColorIdle = constants.fontColorJournalNormalText
        result.userData.textColorOver = constants.fontColorJournalLinkOver
        result.userData.textColorPressed = constants.fontColorJournalLinkPressed
    else
        result.template = templates.journalAlphabetLinkTextUnused
        result.userData.textColorIdle = constants.fontColorInactiveIdle
        result.userData.textColorOver = constants.fontColorInactiveOver
        result.userData.textColorPressed = constants.fontColorInactivePressed
    end
    result.props = {
        text = text,
        textSize = settings.textBookmarkLinkSize()
    }
    if onJournalPageContentMouseMove ~= nil then
        result.events.mouseMove = onJournalPageContentMouseMove
    end
    return result
end

function TXT.createAlphabetTextWidget(text, isUsed, onLetterClickCallback, updateJournalWindow)
    local result = TXT.createBookmarkLinkWidget(text, isUsed, onLetterClickCallback, updateJournalWindow)
    result.userData.letter = text
    result.props.text = "( " .. text .. " )"
    result.props.textSize = settings.textBookmarkAlphabetLetterSize()
    return result
end

function TXT.createContextMenuOptionWidget(text, onClickCallback, updateFunction)
    local result = ui_clickable.createClickableWidget(onClickCallback, updateFunction)
    result.template = templates.contextMenuTextLinkIdle
    result.userData.textColorIdle = constants.paperLikeColor
    result.userData.textColorOver = constants.paperLikeColorLighter
    result.userData.textColorPressed = constants.paperLikeColorPressed
    result.props = {
        text = text
    }
    return result
end

return TXT
