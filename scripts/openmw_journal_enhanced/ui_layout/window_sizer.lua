local util = require('openmw.util')
local ui = require('openmw.ui')
local constants = require("scripts.openmw_journal_enhanced.ui_layout.ui_constants")
local settings = require("scripts.openmw_journal_enhanced.settings")

local resolutionToRecommendedSizeMultiplier = {
    ["(1920, 1080)"] = 2.5,
    ["(1768, 992)"] = 2.5,
    ["(1680, 1050)"] = 2.5,
    ["(1600, 1024)"] = 2.5,
    ["(1600, 900)"] = 2.125,
    ["(1440, 1080)"] = 2.5,
    ["(1440, 900)"] = 2.125,
    ["(1366, 768)"] = 1.875,
    ["(1360, 768)"] = 1.875,
    ["(1280, 1024)"] = 2.125,
    ["(1280, 960)"] = 2.125,
    ["(1280, 800)"] = 1.875,
    ["(1280, 768)"] = 1.875,
    ["(1280, 720)"] = 1.75,
    ["(1176, 664)"] = 1.625,
    ["(1152, 864)"] = 1.875,
    ["(1024, 768)"] = 1.6875,
    ["(800, 600)"] = 1.375,
    ["(720, 576)"] = 1.25,
    ["(720, 480)"] = 1.1875,
    ["(640, 480)"] = 1.125,
}

local WS = {}

function WS.calculateJournalWindowSize()
    local widthMultiplier = settings.journalWindowWidthMultiplier()
    local heightMultiplier = settings.journalWindowHeightMultiplier()

    if settings.useRecommendedResolutionMultipliers() then
        local possibleMultiplier = resolutionToRecommendedSizeMultiplier[tostring(ui.screenSize())]
        if possibleMultiplier ~= nil then
            widthMultiplier = possibleMultiplier
            heightMultiplier = possibleMultiplier
        end
    end

    local possibleWidth = constants.journalTextureWidth * widthMultiplier
    local possibleHeight = constants.journalTextureHeight * heightMultiplier

    return util.vector2(math.floor(possibleWidth), math.floor(possibleHeight))
end

local function calculateNumberOfVisibleBookmarkLines(fontSize)
    local journalWindowHeight = WS.calculateJournalWindowSize().y
    local heightOfScrollableAreaInBookmark = journalWindowHeight
        * constants.bookmarkHeightFactorRelativeToJournalWindowHeight
        * constants.bookmarkScrollableAreaHeightFactorRelativeToBookmarkHeight
    return math.ceil(heightOfScrollableAreaInBookmark / fontSize) - 1 -- minus just in case
end

function WS.calculateNumberOfVisibleBookmarkAlphabetLines()
    return calculateNumberOfVisibleBookmarkLines(settings.textBookmarkAlphabetLetterSize())
end

function WS.calculateNumberOfVisibleBookmarkTopicOrQuestLines()
    return calculateNumberOfVisibleBookmarkLines(settings.textBookmarkLinkSize())
end

return WS