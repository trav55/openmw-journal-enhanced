local storage = require('openmw.storage')
local constants = require("scripts.openmw_journal_enhanced.ui_layout.ui_constants")
local I = require('openmw.interfaces')

local l10nKey = 'openmw_journal_enhanced'
local settingsPageKey = "SettingsCustomOpenmwJournalMainPage"

local S = {}

local settingToStorage = {}

function S.subscribeForChangesIn(settingKey, callbackOnSettingsChange)
    settingToStorage[settingKey]:subscribe(callbackOnSettingsChange)
end

local function floatSetting(settingKey, default, min, storageForThisSetting)
    S[settingKey] = function() return storageForThisSetting:get(settingKey) end
    settingToStorage[settingKey] = storageForThisSetting
    return {
        key = settingKey,
        renderer = 'number',
        name = settingKey,
        description = settingKey .. 'Description',
        default = default,
        argument =
        {
            disabled = false,
            min = min,
            integer = false
        },
    }
end

local function boolSetting(settingKey, default, storageForThisSetting)
    S[settingKey] = function() return storageForThisSetting:get(settingKey) end
    settingToStorage[settingKey] = storageForThisSetting
    return {
        key = settingKey,
        renderer = 'checkbox',
        name = settingKey,
        description = settingKey .. 'Description',
        default = default
    }
end

local function selectSetting(settingKey, default, listOfValues, storageForThisSetting)
    S[settingKey] = function() return storageForThisSetting:get(settingKey) end
    settingToStorage[settingKey] = storageForThisSetting
    return {
        key = settingKey,
        renderer = "select",
        name = settingKey,
        description = settingKey .. 'Description',
        default = default,
        argument = {
            l10n = l10nKey,
            items = listOfValues
        },
    }
end

I.Settings.registerPage({
    key = settingsPageKey,
    l10n = l10nKey,
    name = 'CustomOpenmwJournalModName',
    description = settingsPageKey .. "Description",
})

local journalWindowSettingsKey = "SettingsCustomOpenmwJournal_01JournalWindow"
local storageJournalWindowSettings = storage.playerSection(journalWindowSettingsKey)
I.Settings.registerGroup({
    key = journalWindowSettingsKey,
    page = settingsPageKey,
    l10n = l10nKey,
    name = 'JournalWindowSettings',
    permanentStorage = true,
    settings = {
        boolSetting('useRecommendedResolutionMultipliers', true, storageJournalWindowSettings),
        floatSetting(
            'journalWindowWidthMultiplier',
            constants.journalWindowWidthMultiplier, 0.9,
            storageJournalWindowSettings),
        floatSetting(
            'journalWindowHeightMultiplier',
            constants.journalWindowWidthMultiplier, 0.9,
            storageJournalWindowSettings),
    },
})

local textSettingsKey = "SettingsCustomOpenmwJournal_02Text"
local storageTextSettings = storage.playerSection(textSettingsKey)
I.Settings.registerGroup({
    key = textSettingsKey,
    page = settingsPageKey,
    l10n = l10nKey,
    name = 'TextSettings',
    permanentStorage = true,
    settings = {
        floatSetting('textJournalNormalSize', constants.textJournalNormalSize, 10, storageTextSettings),
        floatSetting('textJournalPageNumberSize', constants.textJournalPageNumberSize, 10, storageTextSettings),
        floatSetting('textJournalButtonSize', constants.textJournalButtonSize, 10, storageTextSettings),
        floatSetting('textBookmarkLinkSize', constants.textBookmarkLinkSize, 10, storageTextSettings),
        floatSetting('textBookmarkAlphabetLetterSize', constants.textBookmarkAlphabetLetterSize, 10, storageTextSettings),
    },
})

local bookmarkSettingsKey = "SettingsCustomOpenmwJournal_03BookmarkScrolling"
local storageBookmarkSettings = storage.playerSection(bookmarkSettingsKey)
I.Settings.registerGroup({
    key = bookmarkSettingsKey,
    page = settingsPageKey,
    l10n = l10nKey,
    name = 'BookmarkSettings',
    permanentStorage = true,
    settings = {
        floatSetting('bookmarkScrollRatio', constants.bookmarkScrollRatio, 0.15, storageBookmarkSettings),
        selectSetting(
            'startingOptionInOptions',
            "startingOptionsChoiceAlphabet",
            { "startingOptionsChoiceAlphabet", "startingOptionsChoiceQuestsActive", "startingOptionsChoiceQuestsAll" },
            storageBookmarkSettings),
        selectSetting(
            'topicEntriesOrder',
            "topicEntriesOrderNewestFirst",
            { "topicEntriesOrderNewestFirst", "topicEntriesOrderOldestFirst" },
            storageBookmarkSettings),
        selectSetting(
            'groupQuestsByCell',
            "groupQuestsByCellChoiceStartingShort",
            {
                "groupQuestsByCellChoiceDisabled",
                "groupQuestsByCellChoiceStartingDetailed",
                "groupQuestsByCellChoiceStartingShort"
            },
            storageBookmarkSettings),
        boolSetting('displayUnnamedQuests', false, storageBookmarkSettings),
    },
})

local tooltipSettingKey = "SettingsCustomOpenmwJournal_04Tooltip"
local storageTooltipSettings = storage.playerSection(tooltipSettingKey)
I.Settings.registerGroup({
    key = tooltipSettingKey,
    page = settingsPageKey,
    l10n = l10nKey,
    name = 'TooltipSettings',
    permanentStorage = true,
    settings = {
        boolSetting('showQuestNamesOnJournalEntryHover', true, storageTooltipSettings),
        boolSetting('showTooltipForContextMenuOption', true, storageTooltipSettings),
        selectSetting(
            'showTopicEntryOnHover',
            "showTopicEntryOnHoverNewest",
            {
                "showTopicEntryOnHoverOldest",
                "showTopicEntryOnHoverNewest",
                "showTopicEntryOnHoverRandom",
                "showTopicEntryOnHoverDisabled"
            },
            storageTooltipSettings),
    },
})

local devSettingsKey = "SettingsCustomOpenmwJournal_10DevSettings"
local storageDevSettings = storage.playerSection(devSettingsKey)
I.Settings.registerGroup({
    key = devSettingsKey,
    page = settingsPageKey,
    l10n = l10nKey,
    name = 'DevSettings',
    permanentStorage = true,
    settings = {
        boolSetting('displayQuestIdInQuestList', false, storageDevSettings),
        boolSetting('isQuestStageEditingEnabled', false, storageDevSettings),
        boolSetting('separateQuestsWithTheSameName', false, storageDevSettings),
        selectSetting(
            'useSampleJournalData',
            "useSmallSampleJournalData",
            { "useBigSampleJournalData", "useSmallSampleJournalData", "noSampleJournalDataUseGameData" },
            storageDevSettings),
        boolSetting('enableVerboseLogging', false, storageDevSettings),
    },
})

return S
