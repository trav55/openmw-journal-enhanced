local self = require('openmw.self')
local calendar = require('openmw_aux.calendar')

local function initializeQuestEntryIfNotPresent(database, questId)
    if database.quests == nil then
        database.quests = {}
    end
    if database.quests[questId] == nil then
        database.quests[questId] = {}
    end
    if database.quests[questId].stages == nil then
        database.quests[questId].stages = {}
    end
end

local NE = {}

function NE.applyNewQuestStartingLocation(database, questId, newLocation)
    initializeQuestEntryIfNotPresent(database, questId)
    database.quests[questId].startingLocation = newLocation
end

function NE.applyQuestUpdate(database, questId, stage)
    initializeQuestEntryIfNotPresent(database, questId)

    local newQuestStageInfo = {
        stageNumber = stage,
        cellWhereStageUpdated = self.cell.name,
        dateWhenStageUpdated = calendar.formatGameTime('*t')
    }
    if newQuestStageInfo.cellWhereStageUpdated == nil or newQuestStageInfo.cellWhereStageUpdated == "" then
        newQuestStageInfo.cellWhereStageUpdated = self.cell.region
    end
    if database.quests[questId].startingLocation == nil or database.quests[questId].startingLocation == "" then
        database.quests[questId].startingLocation = newQuestStageInfo.cellWhereStageUpdated
    end
    table.insert(database.quests[questId].stages, newQuestStageInfo)
end

function NE.debugPrintNonEngineData(database, prefix)
    if database == nil then
        print("custom-openmw-journal[NED]["..prefix.."]: database=nil")
        return
    end
    if database.quests == nil then
        print("custom-openmw-journal[NED]["..prefix.."]: database.quests=nil")
        return
    end
    local numberOfQuests = 0
    for questId, questData in pairs(database.quests) do
        print("custom-openmw-journal[NED]["..prefix.."]: printing quest no. "..numberOfQuests..": " .. questId)
        if questData.stages == nil then
            print("custom-openmw-journal[NED]["..prefix.."]("..questId.."): stages=nil")
        else
            for stageIdx, stageData in pairs(questData.stages) do
                print("custom-openmw-journal[NED]["..prefix.."]("..questId.."):  stage "..stageIdx..": stageNumber="..stageData.stageNumber..
                      ", cellWhereStageUpdated=<" .. stageData.cellWhereStageUpdated .. ">" ..
                      ", day=" .. stageData.dateWhenStageUpdated.day .. ", month=" .. stageData.dateWhenStageUpdated.month .. ", year=" .. stageData.dateWhenStageUpdated.year)
            end
        end
        print("custom-openmw-journal[NED]["..prefix.."]("..questId.."): startingLocation=", questData.startingLocation)
        numberOfQuests = numberOfQuests + 1
    end
end

return NE