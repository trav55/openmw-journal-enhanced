local settings = require("scripts.openmw_journal_enhanced.settings")
local engine_data = require("scripts.openmw_journal_enhanced.gamedata.engine_data")

local function createJournalEntry(id, text, actor, day, month, dayOfMonth)
    return {
        id = id,
        text = text,
        actor = actor,
        day = day,
        month = month,
        dayOfMonth = dayOfMonth
    }
end

local function createJournalTopicEntry(id, text, actor)
    return createJournalEntry(id, text, actor, nil, nil, nil)
end

local function createTopics(database)
    database.topics = {}
    database.topicsSortedByTheirLength = {}
    database.topicsIdxsByTheirFirstCharByte = {}
    local playerTopics = engine_data.getJournalDialogueTopics()
    if playerTopics ~= nil then
        for _, topicData in pairs(playerTopics) do
            database.topics[topicData.id] = {
                topicNameToDisplay = topicData.name,
                entries = {},
            }
            for _, topicEntry in pairs(topicData.entries) do
                table.insert(database.topics[topicData.id].entries,
                    createJournalTopicEntry(topicEntry.id, topicEntry.text, topicEntry.actor))
            end
            table.insert(database.topicsSortedByTheirLength, topicData.id)
        end
    end
    table.sort(database.topicsSortedByTheirLength, function(a, b) return #a > #b end) --longer topics first
    for index, topic in pairs(database.topicsSortedByTheirLength) do
        local firstTopicCharacterByte = string.byte(string.lower(topic), 1, 1)
        if database.topicsIdxsByTheirFirstCharByte[firstTopicCharacterByte] == nil then
            database.topicsIdxsByTheirFirstCharByte[firstTopicCharacterByte] = { index }
        else
            table.insert(database.topicsIdxsByTheirFirstCharByte[firstTopicCharacterByte], index)
        end
    end
end

local function createJournalEntries(database)
    database.journalEntries = {}
    database.numberOfKnownJournalEntries = 0

    local journalEntriesInput = engine_data.getJournalEntriesByDate()
    if journalEntriesInput ~= nil then
        for _, entry in pairs(journalEntriesInput) do
            database.numberOfKnownJournalEntries = database.numberOfKnownJournalEntries + 1
            table.insert(database.journalEntries, createJournalEntry(
                entry.id,
                entry.text,
                entry.actor,
                entry.day,
                entry.month,
                entry.dayOfMonth
            ))
            database.journalEntriesIndexedByUniqueEntryIds[entry.id] = database.numberOfKnownJournalEntries
        end
    end
end

local function createQuestEntries(database)
    local questEntryIndicesByQuestId = {}
    local playerQuestEntries = engine_data.getJournalQuestEntries()
    if playerQuestEntries ~= nil then
        for questIdFromGameData, questEntriesFromGameData in pairs(playerQuestEntries) do
            local questId = string.lower(questIdFromGameData)
            questEntryIndicesByQuestId[questId] = {}
            for _, questEntryFromGameData in pairs(questEntriesFromGameData) do
                table.insert(questEntryIndicesByQuestId[questId], questEntryFromGameData.id)
            end
        end
    end

    database.questEntries = {}
    local groupQuestsWithTheSameNameIntoOne = not settings.separateQuestsWithTheSameName()
    local quests = engine_data.quests()
    if quests ~= nil then
        for _, quest in pairs(quests) do
            local questId = string.lower(quest.id)
            local shouldQuestBeInsertedAsNewEntry = true
            local possibleQuestName = engine_data.questName(quest)
            if possibleQuestName ~= nil and possibleQuestName ~= "" then
                if groupQuestsWithTheSameNameIntoOne then
                    for alreadyGatheredQuestId, alreadyGatheredQuestData in pairs(database.questEntries) do
                        if (questId ~= alreadyGatheredQuestId) and (alreadyGatheredQuestData.name == possibleQuestName) then
                            table.insert(alreadyGatheredQuestData.alternativeIds, questId)
                            if questEntryIndicesByQuestId[questId] then
                                for _, questEntryId in pairs(questEntryIndicesByQuestId[questId]) do
                                    table.insert(alreadyGatheredQuestData.entries, questEntryId)
                                end
                            end
                            alreadyGatheredQuestData.isQuestFinished = (alreadyGatheredQuestData.isQuestFinished or quest.finished)
                            shouldQuestBeInsertedAsNewEntry = false
                            break
                        end
                    end
                end
            else
                possibleQuestName = nil
            end

            if shouldQuestBeInsertedAsNewEntry then
                database.questEntries[string.lower(questId)] = {
                    isQuestFinished = quest.finished,
                    name = possibleQuestName,
                    alternativeIds = {},
                    entries = questEntryIndicesByQuestId[questId] or {},
                    questIdOriginalCase = questId,
                }
            end
        end
    end
end

local function sortQuestEntriesPerJournalEntriesOrder(database)
    if database.journalEntries == nil or database.questEntries == nil then
        return
    end

    local orderingTemplate = {}
    for _, journalEntry in pairs(database.journalEntries) do
        table.insert(orderingTemplate, journalEntry.id)
    end

    for _, quest in pairs(database.questEntries) do
        table.sort(
            quest.entries,
            function(questEntryA, questEntryB)
                local orderA = 0
                local orderB = 0
                for orderingEntryIdx, orderingEntry in pairs(orderingTemplate) do
                    if orderingEntry == questEntryA then
                        orderA = orderingEntryIdx
                    end
                    if orderingEntry == questEntryB then
                        orderB = orderingEntryIdx
                    end
                    if (orderA > 0) and (orderB > 0) then
                        break
                    end
                end
                return orderA < orderB
            end)
    end
end

local function findQuestIdForJournalEntry(journalEntryId, questEntries)
    for questId, quest in pairs(questEntries) do
        for _, entryId in pairs(quest.entries) do
            if journalEntryId == entryId then
                return questId
            end
        end
    end
    return nil
end

local function addQuestIdsToJournalEntries(database)
    if database.journalEntries == nil or database.questEntries == nil then
        return
    end

    for _, journalEntry in pairs(database.journalEntries) do
        journalEntry.questId = findQuestIdForJournalEntry(journalEntry.id, database.questEntries)
    end
end

local JE = {}

function JE.createDatabase(nonEngineData)
    local result = {
        journalEntries = {},
        journalEntriesIndexedByUniqueEntryIds = {},
        numberOfKnownJournalEntries = 0,
        questEntries = {},
        topics = {},
        topicsSortedByTheirLength = {},
        topicsIdxsByTheirFirstCharByte = {},

        nonEngineData = nil,
    }

    function result.updateDatabase(nonEngineData)
        result.nonEngineData = nonEngineData
        createTopics(result)
        createJournalEntries(result)
        createQuestEntries(result)
        sortQuestEntriesPerJournalEntriesOrder(result)
        addQuestIdsToJournalEntries(result)
    end

    function result.getNonEngineDataForQuest(questId)
        if result.nonEngineData ~= nil and result.nonEngineData.quests ~= nil and result.nonEngineData.quests[questId] ~= nil then
            return result.nonEngineData.quests[questId]
        end
        return nil
    end

    result.updateDatabase(nonEngineData)

    return result
end

return JE
