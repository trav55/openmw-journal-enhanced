local core = require('openmw.core')
local self = require('openmw.self')
local types = require('openmw.types')
local settings = require("scripts.openmw_journal_enhanced.settings")

local ED = {}

function ED.getJournalDialogueTopics()
    if settings.useSampleJournalData() == "useBigSampleJournalData" then
        return require("scripts.openmw_journal_enhanced.gamedata.showcase_data.big_topics")
    elseif settings.useSampleJournalData() == "useSmallSampleJournalData" then
        return require("scripts.openmw_journal_enhanced.gamedata.showcase_data.small_topics")
    end
    if types.Player.journal ~= nil and types.Player.journal(self).topics ~= nil then
        -- print(".......TTTSTART topics.........")
        -- local serpent = require("scripts.openmw_journal_enhanced.gamedata.serpent")
        -- print(serpent.dump(types.Player.getJournalDialogueTopics(self)))
        -- print(".......TTTEND topics.........")
        return types.Player.journal(self).topics
    else
        return nil
    end
end

function ED.getJournalEntriesByDate()
    if settings.useSampleJournalData() == "useBigSampleJournalData" then
        return require("scripts.openmw_journal_enhanced.gamedata.showcase_data.big_journal")
    elseif settings.useSampleJournalData() == "useSmallSampleJournalData" then
        return require("scripts.openmw_journal_enhanced.gamedata.showcase_data.small_journal")
    end
    if types.Player.journal ~= nil then
        return types.Player.journal(self).journalTextEntries
    else
        return nil
    end
end

function ED.getJournalQuestEntries() --TODO: with my API this separate listing is unnecessary
    if settings.useSampleJournalData() == "useBigSampleJournalData" then
        return require("scripts.openmw_journal_enhanced.gamedata.showcase_data.big_questentries")
    elseif settings.useSampleJournalData() == "useSmallSampleJournalData" then
        return require("scripts.openmw_journal_enhanced.gamedata.showcase_data.small_questentries")
    end
    if types.Player.journal ~= nil then
        local result = {}
        for _, journalEntry in pairs(types.Player.journal(self).journalTextEntries) do
            if result[journalEntry.questId] == nil then
                result[journalEntry.questId] = {}
            end
            table.insert(result[journalEntry.questId], journalEntry)
        end
        return result
    else
        return nil
    end
end

function ED.quests()
    if settings.useSampleJournalData() == "useBigSampleJournalData" then
        return require("scripts.openmw_journal_enhanced.gamedata.showcase_data.big_quests")
    elseif settings.useSampleJournalData() == "useSmallSampleJournalData" then
        return require("scripts.openmw_journal_enhanced.gamedata.showcase_data.small_quests")
    end
    if types.Player.quests ~= nil then
        -- print(".......TTTSTART quests.........")
        -- local serpent = require("scripts.openmw_journal_enhanced.gamedata.serpent")
        -- local dumpdump = {}
        -- local dddddump_inpout = types.Player.quests(self)
        -- for _, quest in pairs(dddddump_inpout) do
        --     table.insert(dumpdump, {
        --         finished = quest.finished,
        --         id = quest.id,
        --         stage = quest.stage,
        --         started = quest.started,
        --         name = quest.name,
        --     })
        -- end
        -- print(serpent.dump(dumpdump))
        -- print(".......TTTEND quests.........")
        return types.Player.quests(self)
    else
        return nil
    end
end

function ED.questName(quest)
    if core.dialogue.journal ~= nil and core.dialogue.journal.records ~= nil and core.dialogue.journal.records[quest.id] ~= nil then
        return core.dialogue.journal.records[quest.id].questName
    else
        return nil
    end
end

function ED.questJournalEntryRecords(questId)
    if core.dialogue.journal ~= nil and core.dialogue.journal.records ~= nil and core.dialogue.journal.records[questId] ~= nil then
        return core.dialogue.journal.records[questId].infos
    else
        return {}
    end
end

function ED.questJournalRecords()
    if core.dialogue.journal ~= nil and core.dialogue.journal.records ~= nil then
        return core.dialogue.journal.records
    else
        return {}
    end
end

return ED
