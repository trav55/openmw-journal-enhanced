local PhraseType = require("scripts.openmw_journal_enhanced.wording.phrase_type")
local character_sizing = require("scripts.openmw_journal_enhanced.wording.character_sizing")
local ui_text = require("scripts.openmw_journal_enhanced.ui_layout.ui_text")

local createTextWidget = {
    [PhraseType.TOPIC] = ui_text.createLinkTextWidget,
    [PhraseType.NORMAL_TEXT] = ui_text.createNormalTextWidget,
    [PhraseType.WHITESPACE] = ui_text.createNormalTextWidget,
    [PhraseType.PUNCTUATION] = ui_text.createNormalTextWidget,
    [PhraseType.HEADER] = ui_text.createHeaderTextWidget,
    [PhraseType.NEWLINE] = ui_text.createNormalTextWidget,
    [PhraseType.DOUBLE_NEWLINE] = ui_text.createNormalTextWidget
}

local function addPhraseToTable(targetTable, text, phraseType, hashedWordWidths, tooltipDataForThisPhraseWidget)
    if (hashedWordWidths == nil) then
        hashedWordWidths = {}
    end
    if (hashedWordWidths[text] == nil) then
        hashedWordWidths[text] = character_sizing.calculateWidth(text)
    end

    local result = createTextWidget[phraseType](text)
    if result.userData == nil then
        result.userData = {}
    end
    result.userData.type = phraseType
    result.userData.width = hashedWordWidths[text]
    result.userData.tooltipData = tooltipDataForThisPhraseWidget

    table.insert(targetTable, result)
end

local PATTERN_FOR_WORD_END = "^[%.%?!,;%s\"]"
local PATTERN_FOR_WHITESPACE = "^%s"
local PATTERN_FOR_PUNCTUATION = "^[%.%?!,;\"]"
local PATTERN_FOR_DOUBLE_NEWLINE = "^[\n\r][\n\r]"
local PATTERN_FOR_NEWLINE = "^[\n\r]"

local WSP = {
    HEADER_START = "<h1>",
    HEADER_END = "</h1>"
}

local PATTERN_FOR_HEADER_START = "^" .. WSP.HEADER_START
local PATTERN_FOR_HEADER_END = WSP.HEADER_END

local function isTopicFittingIntoThisPlace(topic, lowercaseTextCopy, currentCharacterStep)
    -- This is basically
    -- return string.match(lowercaseTextCopy, "^" .. topic, currentCharacterStep)
    -- but the version below is sufficient for my needs and runs 2x faster
    local lengthOfThisTopic = #topic
    if lengthOfThisTopic > (#lowercaseTextCopy - currentCharacterStep) then
        return false
    end
    local found = false
    local topicCharacterIdxCheckedNow = 1
    while (topicCharacterIdxCheckedNow <= lengthOfThisTopic) do
        local textPositionCheckedNow = (currentCharacterStep + topicCharacterIdxCheckedNow - 1)
        local thisCharacterMatches = (
            string.byte(lowercaseTextCopy, textPositionCheckedNow, textPositionCheckedNow)
            ==
            string.byte(topic, topicCharacterIdxCheckedNow, topicCharacterIdxCheckedNow)
        )
        if (not thisCharacterMatches) then
            break
        end
        if thisCharacterMatches and (topicCharacterIdxCheckedNow == lengthOfThisTopic) then
            found = true
        end
        topicCharacterIdxCheckedNow = topicCharacterIdxCheckedNow + 1
    end
    return found
end

function WSP.splitToPhraseWidgets(text, gameData, hashedWordWidths, tooltipDataForThisPhraseWidget)
    local splitPhrases = {}

    local lowercaseTextCopy = string.lower(text)

    local phraseStart = 1
    local currentCharacterStep = 1
    while currentCharacterStep <= #text do
        local nextStepLength = 1
        local foundPhraseAlready = false

        local thisCharacterByte = string.byte(lowercaseTextCopy, currentCharacterStep, currentCharacterStep)

        -- try to find topic at this position
        -- the most time-consuming part of this function. I ran out of ideas on how to make it faster
        if gameData.topicsIdxsByTheirFirstCharByte[thisCharacterByte] ~= nil then
            for _, topicIdx in pairs(gameData.topicsIdxsByTheirFirstCharByte[thisCharacterByte]) do
                local topic = gameData.topicsSortedByTheirLength[topicIdx]
                local found = isTopicFittingIntoThisPlace(topic, lowercaseTextCopy, currentCharacterStep)
                if found then
                    if phraseStart <= currentCharacterStep - 1 then
                        addPhraseToTable(
                            splitPhrases,
                            string.sub(text, phraseStart, currentCharacterStep - 1),
                            PhraseType.NORMAL_TEXT,
                            hashedWordWidths,
                            tooltipDataForThisPhraseWidget)
                    end
                    addPhraseToTable(
                        splitPhrases,
                        string.sub(text, currentCharacterStep, currentCharacterStep + #topic - 1),
                        PhraseType.TOPIC,
                        hashedWordWidths,
                        tooltipDataForThisPhraseWidget)
                    nextStepLength = #topic
                    phraseStart = currentCharacterStep + #topic
                    foundPhraseAlready = true
                    break
                end
            end
        end

        -- determine if text is in header
        if string.match(lowercaseTextCopy, PATTERN_FOR_HEADER_START, currentCharacterStep) then
            local findEnd = string.find(lowercaseTextCopy, PATTERN_FOR_HEADER_END, currentCharacterStep)
            if findEnd then
                addPhraseToTable(
                    splitPhrases,
                    string.sub(text, currentCharacterStep + #WSP.HEADER_START, findEnd - 1),
                    PhraseType.HEADER,
                    hashedWordWidths,
                    tooltipDataForThisPhraseWidget)
            end
            nextStepLength = (findEnd + #WSP.HEADER_END) - currentCharacterStep
            phraseStart = currentCharacterStep + nextStepLength
            foundPhraseAlready = true
        end

        if not foundPhraseAlready then
            if string.match(lowercaseTextCopy, PATTERN_FOR_WORD_END, currentCharacterStep) then
                if phraseStart < currentCharacterStep then
                    addPhraseToTable(
                        splitPhrases,
                        string.sub(text, phraseStart, currentCharacterStep - 1),
                        PhraseType.NORMAL_TEXT,
                        hashedWordWidths,
                        tooltipDataForThisPhraseWidget)
                end
                if string.match(lowercaseTextCopy, PATTERN_FOR_DOUBLE_NEWLINE, currentCharacterStep) then
                    addPhraseToTable(
                        splitPhrases,
                        "\n",
                        PhraseType.DOUBLE_NEWLINE,
                        hashedWordWidths,
                        tooltipDataForThisPhraseWidget)
                    nextStepLength = 2
                elseif string.match(lowercaseTextCopy, PATTERN_FOR_NEWLINE, currentCharacterStep) then
                    addPhraseToTable(
                        splitPhrases,
                        "",
                        PhraseType.NEWLINE,
                        hashedWordWidths,
                        tooltipDataForThisPhraseWidget)
                elseif string.match(lowercaseTextCopy, PATTERN_FOR_WHITESPACE, currentCharacterStep) then
                    addPhraseToTable(
                        splitPhrases,
                        string.sub(text, currentCharacterStep, currentCharacterStep),
                        PhraseType.WHITESPACE,
                        hashedWordWidths,
                        tooltipDataForThisPhraseWidget)
                elseif string.match(lowercaseTextCopy, PATTERN_FOR_PUNCTUATION, currentCharacterStep) then
                    addPhraseToTable(
                        splitPhrases,
                        string.sub(text, currentCharacterStep, currentCharacterStep),
                        PhraseType.PUNCTUATION,
                        hashedWordWidths,
                        tooltipDataForThisPhraseWidget)
                end
                phraseStart = currentCharacterStep + nextStepLength
            elseif currentCharacterStep == #text then
                addPhraseToTable(
                    splitPhrases,
                    string.sub(text, phraseStart, #text),
                    PhraseType.NORMAL_TEXT,
                    hashedWordWidths,
                    tooltipDataForThisPhraseWidget)
            end
        end

        currentCharacterStep = currentCharacterStep + nextStepLength
    end

    return splitPhrases
end

return WSP
