local word_splitting = require('scripts.openmw_journal_enhanced.wording.phrase_splitting')
local l10n = require('openmw.core').l10n("openmw_journal_enhanced")
local calendar = require('openmw_aux.calendar')
local settings = require("scripts.openmw_journal_enhanced.settings")
local tooltip_data = require("scripts.openmw_journal_enhanced.outside_manipulators.tooltip_data")
local core = require('openmw.core')

local function makeJournalEntryHeaderText(entry)
    return tostring(entry.dayOfMonth) ..
        " " .. calendar.monthNameInGenitive(entry.month) .. " (" .. l10n("Day") .. " " .. tostring(entry.day) .. ")"
end

local function makeQuestEntryHeaderText(questData, fallbackOutput)
    if questData ~= nil and questData.name ~= nil and questData.name ~= "" then
        return questData.name
    elseif fallbackOutput ~= nil then
        return fallbackOutput
    else
        return "ERROR_QUEST_ID_EXPECTED"
    end
end

local function makeTopicOrQuestNameHeader(headerText)
    return word_splitting.HEADER_START .. headerText .. word_splitting.HEADER_END .. "\n"
end

local function createTooltipDataForQuestHeader(gameData, questId)
    local questContextText = l10n("Quest") ..
        ": \"" .. makeQuestEntryHeaderText(gameData.questEntries[questId], questId) .. "\""
    if settings.displayQuestIdInQuestList() then
        questContextText = questContextText .. " [" .. questId .. "]"
    end
    local tooltipForQuest = tooltip_data.makeTooltipData(
        questContextText,
        l10n("ContextMenuTooltip01") .. l10n("ContextMenuTooltip02") .. l10n("ContextMenuTooltip03"),
        {
            settingObstructingTooltip = "showQuestNamesOnJournalEntryHover",
            settingObstructingSecondaryTooltip = "showTooltipForContextMenuOption",
            questId = questId
        })
    return tooltipForQuest
end

local function addJournalEntryHeader(outputTable, prefix, journalEntry, gameData, hashedWordWidths,
                                     rightClickCallbackCreatingContextMenu)
    local input = prefix .. makeTopicOrQuestNameHeader(makeJournalEntryHeaderText(journalEntry))

    local tooltipForQuest = createTooltipDataForQuestHeader(gameData, journalEntry.questId)

    for _, splitPhraseForThisEntry in pairs(word_splitting.splitToPhraseWidgets(input, gameData, hashedWordWidths, tooltipForQuest)) do
        splitPhraseForThisEntry.userData.rightClickCallback = rightClickCallbackCreatingContextMenu
        table.insert(outputTable, splitPhraseForThisEntry)
    end
end

local noIdeaForTooltipForThis = nil

local BC = {}

function BC.generateJournalEntriesSplitIntoPhrases(gameData, hashedWordWidths, rightClickCallbackCreatingContextMenu)
    local shouldBenchmark = settings.enableVerboseLogging()
    local result = {}

    local benchmarkStart = core.getRealTime()
    local benchmarkingsplitToPhraseWidgets = {}
    if shouldBenchmark then
        print("BENCHMARK: generateJournalEntriesSplitIntoPhrases START:", benchmarkStart)
    end

    for entryIdx, entry in ipairs(gameData.journalEntries) do
        local prefix = ""
        if entryIdx > 1 then
            prefix = prefix .. "\n\n"
        end

        addJournalEntryHeader(result, prefix, entry, gameData, hashedWordWidths, rightClickCallbackCreatingContextMenu)

        if shouldBenchmark then benchmarkingsplitToPhraseWidgets[entryIdx] = { bstart = core.getRealTime() } end

        for _, splitPhraseForThisEntry in pairs(word_splitting.splitToPhraseWidgets(entry.text, gameData, hashedWordWidths)) do
            table.insert(result, splitPhraseForThisEntry)
        end

        if shouldBenchmark then benchmarkingsplitToPhraseWidgets[entryIdx].bend = core.getRealTime() end
    end

    if shouldBenchmark then
        local benchmarkEnd = core.getRealTime()
        print("BENCHMARK: generateJournalEntriesSplitIntoPhrases END:", benchmarkEnd)

        local avgTimeTakenPerSplitCall = 0
        for _, bmarksOnThisCall in pairs(benchmarkingsplitToPhraseWidgets) do
            avgTimeTakenPerSplitCall = avgTimeTakenPerSplitCall + (bmarksOnThisCall.bend - bmarksOnThisCall.bstart)
        end
        avgTimeTakenPerSplitCall = (avgTimeTakenPerSplitCall / #benchmarkingsplitToPhraseWidgets)

        print("-- overall:", (benchmarkEnd - benchmarkStart), ", avg per split call:", avgTimeTakenPerSplitCall,
            ", calls:", #benchmarkingsplitToPhraseWidgets, "--")
    end

    return result
end

function BC.generateEntriesForTopic(gameData, topic, hashedWordWidths)
    local result = {}
    for _, splitPhrasesForThisEntry in pairs(word_splitting.splitToPhraseWidgets(makeTopicOrQuestNameHeader(topic), gameData, hashedWordWidths)) do
        table.insert(result, splitPhrasesForThisEntry)
    end

    local lowerCaseTopicForSearching = string.lower(topic)

    if gameData.topics[lowerCaseTopicForSearching] ~= nil then
        local entries = gameData.topics[lowerCaseTopicForSearching].entries
        local startingIterationPosition = 1
        local endingIterationPosition = #entries
        local iteratingValue = 1

        local sortingOrder = settings.topicEntriesOrder()
        if sortingOrder == "topicEntriesOrderNewestFirst" then
            startingIterationPosition = #entries
            endingIterationPosition = 1
            iteratingValue = -1
        end

        for entryIdx = startingIterationPosition, endingIterationPosition, iteratingValue do
            local input = ""
            if entryIdx ~= startingIterationPosition then
                input = input .. "\n\n"
            end
            input = input .. entries[entryIdx].actor
            input = input .. ", \"" .. entries[entryIdx].text .. "\""
            for _, splitPhrasesForThisEntry in pairs(word_splitting.splitToPhraseWidgets(input, gameData, hashedWordWidths, noIdeaForTooltipForThis)) do
                table.insert(result, splitPhrasesForThisEntry)
            end
        end
    end

    return result
end

function BC.generateEntriesForSingleQuest(gameData, questId, hashedWordWidths, rightClickCallbackCreatingContextMenu)
    local result = {}
    local questHeaderText = makeQuestEntryHeaderText(gameData.questEntries[questId], questId)

    local tooltipForQuest = createTooltipDataForQuestHeader(gameData, questId)

    for _, splitPhraseForThisEntry in pairs(
        word_splitting.splitToPhraseWidgets(
            makeTopicOrQuestNameHeader(questHeaderText),
            gameData,
            hashedWordWidths,
            tooltipForQuest)) do
        splitPhraseForThisEntry.userData.rightClickCallback = rightClickCallbackCreatingContextMenu
        table.insert(result, splitPhraseForThisEntry)
    end

    for entryIdx, entryId in pairs(gameData.questEntries[questId].entries) do
        local input = ""
        if entryIdx > 1 then
            input = input .. "\n\n"
        end
        input = input .. gameData.journalEntries[gameData.journalEntriesIndexedByUniqueEntryIds[entryId]].text
        for _, splitPhrasesForThisEntry in pairs(word_splitting.splitToPhraseWidgets(input, gameData, hashedWordWidths, noIdeaForTooltipForThis)) do
            table.insert(result, splitPhrasesForThisEntry)
        end
    end

    return result
end

return BC
