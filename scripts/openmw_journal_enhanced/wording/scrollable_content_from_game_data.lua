local ui_text = require("scripts.openmw_journal_enhanced.ui_layout.ui_text")
local pairsByKeys = require("scripts.openmw_journal_enhanced.utils.pairs_by_keys").pairsByKeys
local settings = require("scripts.openmw_journal_enhanced.settings")
local l10n = require('openmw.core').l10n("openmw_journal_enhanced")
local tooltip_data = require("scripts.openmw_journal_enhanced.outside_manipulators.tooltip_data")

local function trimCellLocation(cellName)
    return string.gsub(cellName, "^(.+),.*", "%1", 1)
end

local function sortLinkWidgets(widgetA, widgetB)
    return widgetA.props.text < widgetB.props.text
end

local BC = {}

function BC.generateTopicListForLetter(letter, gameData, callbacks)
    local result = {}
    if gameData.topicsIdxsByTheirFirstCharByte == nil or gameData.topicsIdxsByTheirFirstCharByte[string.byte(string.lower(letter))] == nil then
        return result
    end
    for _, topicIdx in pairs(gameData.topicsIdxsByTheirFirstCharByte[string.byte(string.lower(letter))]) do
        local topic = gameData.topicsSortedByTheirLength[topicIdx]
        local topicLink = ui_text.createBookmarkLinkWidget(
            gameData.topics[topic].topicNameToDisplay,
            true,
            callbacks.changeJournalToSpecificTopicView,
            callbacks.updateJournalWindow,
            callbacks.onJournalPageContentMouseMove)
        table.insert(result, topicLink)
    end
    table.sort(result, sortLinkWidgets)
    return result
end

function BC.generateTopicListForSubstring(filter, gameData, callbacks)
    local pattern = string.lower(filter)
    local result = {}
    for _, topic in pairs(gameData.topicsSortedByTheirLength) do
        if string.find(topic, pattern, 1, true) then
            local topicLink = ui_text.createBookmarkLinkWidget(
                gameData.topics[topic].topicNameToDisplay,
                true,
                callbacks.changeJournalToSpecificTopicView,
                callbacks.updateJournalWindow,
                callbacks.onJournalPageContentMouseMove)
            table.insert(result, topicLink)
        end
    end
    table.sort(result, sortLinkWidgets)
    return result
end

function BC.generateQuestList(gameData, callbacks, questFilteringInformation)
    local result = {}
    local pattern = nil
    if questFilteringInformation.questNameFilter and #questFilteringInformation.questNameFilter >= 2 then
        pattern = string.lower(questFilteringInformation.questNameFilter)
    end

    local startingOption = settings.groupQuestsByCell()
    local displayUnnamedQuests = settings.displayUnnamedQuests()
    local displayQuestIdInQuestList = settings.displayQuestIdInQuestList()
    local questWidgetsForFurtherModification = {}
    for questId, questData in pairs(gameData.questEntries) do
        local nameUsedForPatternMatchingAndDisplay = (questData.name or questId)
        if displayQuestIdInQuestList then
            nameUsedForPatternMatchingAndDisplay = nameUsedForPatternMatchingAndDisplay .. ' [' .. questId
            for _, alternativeQuestId in pairs(questData.alternativeIds) do
                nameUsedForPatternMatchingAndDisplay = nameUsedForPatternMatchingAndDisplay .. ', ' .. alternativeQuestId
            end
            nameUsedForPatternMatchingAndDisplay = nameUsedForPatternMatchingAndDisplay .. ']'
        end

        local isQuestActive = not questData.isQuestFinished
        local shouldQuestBeDisplayedIfUnnamed = (questData.name ~= nil)
            or ((questData.name == nil) and displayUnnamedQuests)
        local isQuestMatchingPattern = (pattern == nil)
            or (string.find(string.lower(nameUsedForPatternMatchingAndDisplay), pattern, 1, true))
        if (shouldQuestBeDisplayedIfUnnamed and isQuestMatchingPattern) and (questFilteringInformation.shouldShowActiveAndInactiveQuests or isQuestActive) then
            local questEntry = ui_text.createBookmarkLinkWidget(
                nameUsedForPatternMatchingAndDisplay,
                isQuestActive,
                callbacks.changeJournalToSingleQuestView,
                callbacks.updateJournalWindow,
                callbacks.onJournalPageContentMouseMove)
            questEntry.props.visible = true
            questEntry.userData.questId = questId
            questEntry.userData.rightClickCallback = callbacks.rightClickCreateContextMenu

            questEntry.userData.tooltipData = tooltip_data.makeTooltipData(
                nameUsedForPatternMatchingAndDisplay,
                l10n("ContextMenuTooltip01") .. l10n("ContextMenuTooltip02") .. l10n("ContextMenuTooltip03"),
                {
                    isQuestListQuestElement = true,
                    settingObstructingTooltip = "showTooltipForContextMenuOption"
                })

            if startingOption ~= "groupQuestsByCellChoiceDisabled" then
                table.insert(questWidgetsForFurtherModification, questEntry)
            else
                table.insert(result, questEntry)
            end
        end
    end

    if #questWidgetsForFurtherModification == 0 then
        table.sort(result, sortLinkWidgets)
        return result
    end

    table.sort(questWidgetsForFurtherModification, sortLinkWidgets)
    local unknownLocationKey = l10n("UnknownLocation")
    local questWidgetsByLocation = {}
    for _, questWidget in pairs(questWidgetsForFurtherModification) do
        local possibleLocation = unknownLocationKey

        local nonEngineData = gameData.getNonEngineDataForQuest(questWidget.userData.questId)
        if nonEngineData ~= nil and nonEngineData.startingLocation ~= nil and nonEngineData.startingLocation ~= "" then
            if startingOption == "groupQuestsByCellChoiceStartingShort" then
                possibleLocation = trimCellLocation(nonEngineData.startingLocation)
            else
                possibleLocation = nonEngineData.startingLocation
            end
        end

        if questWidgetsByLocation[possibleLocation] == nil then
            questWidgetsByLocation[possibleLocation] = {}
        end
        table.insert(questWidgetsByLocation[possibleLocation], questWidget)
    end

    for location, quests in pairsByKeys(questWidgetsByLocation) do
        table.insert(result, ui_text.createHeaderTextWidget(location .. ":"))
        for _, questWidget in pairs(quests) do
            table.insert(result, questWidget)
        end
    end

    return result
end

return BC
