return {
    TOPIC = 1,
    NORMAL_TEXT = 2,
    WHITESPACE = 3,
    PUNCTUATION = 4,
    NEWLINE = 5,
    HEADER = 6,
    DOUBLE_NEWLINE = 7
}