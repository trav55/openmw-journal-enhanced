local l10n = require('openmw.core').l10n("openmw_journal_enhanced")
if require('openmw.core').API_REVISION < 62 then
    error(l10n("CustomOpenmwJournalModName") .. l10n("openMwVersionTooEarlyForThisCustomOpenmwJournalMod"))
end

local I = require('openmw.interfaces')
local journalLayout = require("scripts.openmw_journal_enhanced.window.journal")
local mousewheel = require("scripts.openmw_journal_enhanced.window.mousewheel_handler")
local quest_location_naming = require("scripts.openmw_journal_enhanced.outside_manipulators.quest_location_naming")
local non_engine_data = require("scripts.openmw_journal_enhanced.gamedata.non_engine_data")
local settings = require("scripts.openmw_journal_enhanced.settings")
local tooltip = require("scripts.openmw_journal_enhanced.outside_manipulators.tooltip")
local context_menu = require("scripts.openmw_journal_enhanced.outside_manipulators.context_menu")
local quest_stage_editing = require("scripts.openmw_journal_enhanced.outside_manipulators.quest_stage_editing")

local nonEngineGameData = {}
local journalWindow = journalLayout.createJournalWindow(nonEngineGameData)

local function updateJournalInPlayerScript()
    journalWindow:update()
    tooltip.killTooltip()
    context_menu.killContextMenu()
end

local function replaceVanillaJournalWithNewJournal()
    I.UI.registerWindow(
        "Journal",
        function()
            if journalWindow.layout.props.visible then return end
            journalLayout.updateJournalWindowOnOpen(journalWindow.layout, nonEngineGameData)
            journalWindow.layout.props.visible = true
            updateJournalInPlayerScript()
        end,
        function()
            journalWindow.layout.props.visible = false
            quest_location_naming.removeQuestRenameWindow()
            quest_stage_editing.removeQuestStageEditWindow()
            updateJournalInPlayerScript()
        end
    )
end

return {
    engineHandlers = {
        onLoad = function(savedData)
            if savedData ~= nil then
                nonEngineGameData = savedData
            end
            if settings.enableVerboseLogging() then
                non_engine_data.debugPrintNonEngineData(nonEngineGameData, "onLoad-SavedData")
                non_engine_data.debugPrintNonEngineData(nonEngineGameData, "onLoad-ned")
            end
        end,
        onSave = function()
            if settings.enableVerboseLogging() then
                non_engine_data.debugPrintNonEngineData(nonEngineGameData, "onSave")
            end
            return nonEngineGameData
        end,
        onQuestUpdate = function(questId, stage)
            non_engine_data.applyQuestUpdate(nonEngineGameData, questId, stage)
            if settings.enableVerboseLogging() then
                non_engine_data.debugPrintNonEngineData(nonEngineGameData, "afterOnQuestUpdate")
            end
        end,
        onActive = function()
            replaceVanillaJournalWithNewJournal()
        end,
        onMouseWheel = function(x, y)
            if not journalWindow.layout.props.visible or x == 0 then return end
            mousewheel.manipulateJournalBasedOnMouseWheelTurn(journalWindow, x)
            updateJournalInPlayerScript()
        end,
    }
}
